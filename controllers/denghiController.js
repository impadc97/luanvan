var controller = {};
var models = require('../models');
controller.getAll = function (callback) {
    models.denghi
        .findAll({ raw: true })
        .then(function (denghi) {
            callback(denghi);
        });
};

controller.getAllforQTLB = function (idNvQttb, callback) {
    models.denghi
        .findAll({
            where: { idNguoiXulyDon: null }
        })
        .then(function (dsDenghiChuaduyet) {
            models.denghi
                .findAll({
                    where: { idNguoiXulyDon: idNvQttb }
                })
                .then(function (dsDenghiNvSohuu) {
                    let data = {
                        dsDenghiChuaduyet: dsDenghiChuaduyet,
                        dsDenghiNvSohuu: dsDenghiNvSohuu
                    }
                    callback(data);
                })
        })
}

controller.getByID = function (id, callback) {
    models.denghi
        .findOne({
            where: { id: id }
        })
        .then(function (denghi) {
            callback(denghi);
        })
}

// Tat ca thong tin ve don nghi va thong tin ve nguoi de xuat don
controller.getDetail = function (id, callback) {
    models.denghi
        .findOne({
            where: { id: id }
        })
        .then(function (denghi) {
            models.Users.findOne({
                where: { id: denghi.IdNguoiYeucau }
            })
                .then(function (nguoidedon) {
                    models.Users.findOne({
                        where: { id: denghi.idNguoiXulyDon }
                    })
                        .then(function (nhanvienXulydon) {
                            let data = {
                                denghi: denghi,
                                nguoidedon: nguoidedon,
                                nhanvienXulydon: nhanvienXulydon
                            };
                            callback(data);
                        })
                })
        })
}

controller.getByOwnerID = function (ownerId, callback) {
    models.denghi
        .findAll({
            where: { IdNguoiYeucau: ownerId }
        })
        .then(function (denghi) {
            callback(denghi);
        })
}

// controller.createDeNghisuachua = function (data,idthietbicansua, callback) {
//     models.denghi
//         .create(data)
//         .then(function (insertedDenghi) {
//             let ctdsc={};
//             ctdsc.idThietBi = idthietbicansua;
//             ctdsc.idDonDenghi = insertedDenghi.id;
//             models.chitietdonsuachua.create(ctdsc)
//             .then(function(rs){
//                 callback(200);
//             })
//         })

// }

controller.createDeNghi = function (data, callback) {
    models.denghi
        .create(data)
        .then(function () {
            callback();
        })
        .catch(function (err) {
            callback(err);
        })
}

controller.updateDeNghi = function (data, callback) {
    models.denghi
        .update({
            NoiDungHoSo: data.NoiDungHoSo,
            NgayDeNghi: data.NgayDeNghi,
            NgayNhanDeNghi: data.NgayNhanDeNghi,
            NamThucHien: data.NamThucHien,
            DaThucHien: data.DaThucHien,
        }, {
                where: { id: data.id }
            }).then(function (data) {
                callback(data);
            });
}
controller.getPhongBan = function (callback) {
    models.phongban
        .findAll({ raw: true })
        .then(function (data) {
            callback(data);
        })
}
controller.getBoMon = function (id, callback) {
    models.bomon
        .findAll({
            where: { IDPhongBan: id }
        })
        .then(function (data) {
            callback(data);
        })
}
controller.getAllBoMon = function (callback) {
    models.bomon
        .findAll({
            raw: true
        })
        .then(function (data) {
            callback(data);
        })
}
controller.getBoMonbyID = function (id, callback) {
    models.bomon
        .findOne({
            where: { id: id },
        })
        .then(function (data) {
            callback(data);
        })
}
controller.updateNguoixulyDon = function (idNhanvienQltb, idDenghi, callback) {
    let curDate = new Date(Date.now());
    models.denghi
        .update(
            {
                Tinhtrang: 2, //Hoan tat tinh trang 1, chuyen qua giai doan 2
                idNguoiXulyDon: idNhanvienQltb,
                NgayNhan: curDate
            },
            { where: { id: idDenghi } }
        )
        .then(rs => callback(rs));
}
controller.nextStepTly=function(idDeNghi,curstep,callback){
    let nextstep=curstep+1;
    models.denghi
    .update({
        Tinhtrang:nextstep
    },
    {where:{id:idDeNghi}})
    .then(function(){
        return models.denghi
        .findOne({
            where: { id: idDeNghi }
        })
        .then(function (denghi) {
            callback(denghi);
        })
    });
}
controller.nextStep = function (idDenghi, stepcontent, callback) {
    let nextStatus = stepcontent.curStatus + 1;

    switch (stepcontent.curStatus) {
        case 2: {
            let DonDenghiFinalImgPath = stepcontent.DonDenghiFinalImgPath;
            models.denghi
                .update(
                    {
                        Tinhtrang: nextStatus,
                        DonDenghiFinalImgPath: DonDenghiFinalImgPath,
                    },
                    { where: { id: idDenghi } }
                ).then(rs => callback(rs));
        }
        default: {
            models.denghi
                .update(
                    {
                        Tinhtrang: nextStatus
                    },
                    { where: { id: idDenghi } }
                ).then(rs => callback(rs));
        } break;
    }
}

controller.luudsbaogia = function (idDenghi, DsBaogiaPath, callback) {

    models.denghi
        .update(
            {
                DsBaogiaPath: DsBaogiaPath
            },
            { where: { id: idDenghi } }
        ).then(rs => callback(rs));
}

controller.chonBaogia = function (idDenghi, BaogiaFinalPath, callback) {
    models.denghi
        .update(
            {
                BaogiaFinalPath: BaogiaFinalPath
            },
            { where: { id: idDenghi } }
        ).then(rs => callback(rs));
}
controller.updateNgayThuHoi=function(idDenghi,NgayThuHoi,callback){
    models.denghi
    .update(
        {
            NgayThuHoi:NgayThuHoi
        },
        {where:{id:idDenghi}}
    ).then(rs => callback(rs));
}
controller.huydon = function (idDenghi,lydohuydon, callback){
    let cancelstatus = 0;
    models.denghi
        .update(
            {
                Tinhtrang: cancelstatus,
                LyDoHuyDon: lydohuydon
            },
            { where: { id: idDenghi } }
        ).then(rs => callback(rs));
}
controller.huylich=function(idDenghi,callback){
    models.denghi
    .update({
        NgayThuHoi:null
    },{where:
        {id:idDenghi}
    })
    .then(rs=>callback(rs));
}

module.exports = controller;