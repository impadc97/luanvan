var controller = {};
var models = require('../models');

controller.getAll = function (callback) {
    models.bomon
        .findAll({ raw: true })
        .then(function (bomons) {
            callback(bomons);
        });
};

controller.getByIdPhongBan = function (idPhongban, callback){
    models.bomon
        .findAll({where:{IDPhongBan: idPhongban}})
        .then(function(bomons){
            callback(bomons);
        })
}

module.exports = controller;