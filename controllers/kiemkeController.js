var controller = {};
var sequelize = require('sequelize');
var models = require('../models');
controller.getAll = function (callback) {
    models.dexuatkiemke
        .findAll({
            include:[
                {model: models.Users,as:'NguoiTao'},
                {model: models.Users,as:'NguoiDuyet'},
                {model: models.chitietdexuatkiemke},
            ]
        })
        .then(function (dexuatkiemkes) {
            //console.log(dexuatkiemkes)
            callback(dexuatkiemkes);
        });
};

controller.createDXKK = function (data, callback) {
    console.log(data);
    models.dexuatkiemke
        .create(data)
        .then(function (rs) {
            console.log(data);
            callback();
        })
        .catch(function (err) {
            callback(err);
        })
}

module.exports = controller;