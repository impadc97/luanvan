var controller = {};
var models = require('../models');
var bcrypt = require('bcryptjs');
controller.createUser = function (user, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(user.password, salt, function (err, hash) {
            user.password = hash;
            models.Users
                .create(user)
                .then(function () {
                    callback(err);
                });
        });
    });
};
controller.changePwd = function (id, newpass, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(newpass, salt, function (err, hash) {
            newpass = hash;
            models.Users
                .update({
                    password: newpass
                }, { where: { id: id } })
                .then(function () {
                    callback(err);
                })
        })
    })
}
controller.comparePassword = function (password, hash, callback) {
    bcrypt.compare(password, hash, function (err, isMatch) {
        if (err) throw err;
        callback(null, isMatch);
    });
};
controller.getUserByUsername = function (username, callback) {
    let Obj = { username: username }
    models.Users.findOne({ where: Obj })
        .then(function (user) {
            callback(false, user);
        })
        .catch(function (err) {
            callback(err, null);
        });
};
controller.getUserById = function (id, callback) {
    models.Users.findOne({ where: { id: id } })
        .then(function (user) {
            callback(false, user);
        });
}
controller.getAll = function (callback) {
    models.Users
        .findAll()
        .then(function (users) {
            models.phongban
            .findAll()
            .then(function(phongbans){
                var data = {
                    users : users,
                    phongbans : phongbans
                };
                callback(data);
            })
        })
};
controller.ban = function (id, callback) {
    models.Users.update({
        isActive: false
    }, {
            where: { id: id }
        }).then(function (user) {
            callback(user)
        });
}
controller.updateUser = function (user, callback) {
    models.Users.update({
        username: user.username,
        password: user.password,
        hoten: user.hoten,
        phone: user.phone,
        email: user.email,
        isActive: user.isActive,
        isAdmin: user.isAdmin,
        role: user.role,
        idPhongban: user.idPhongban,
    },
        {
            where: { id: user.id }
        }).then(function (user) {
            callback(user)
        });
}
module.exports = controller;