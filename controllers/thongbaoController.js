var controller = {};
var models = require('../models');
controller.getAll=function (callback)
{
    models.thongbao
    .findAll({raw:true})
    .then(function(thongbao){
        callback(thongbao);
    });
};
controller.getByID=function(id,callback){
    models.thongbao
    .findOne({
        where:{id:id}
    })
    .then(function(thongbao){
        callback(thongbao);
    })
}
controller.createThongbao=function(data,callback){
    models.thongbao
    .create(data)
    .then(function () {
        callback();
    })
    .catch(function (err) {
        callback(err);
    })
}
controller.getByIDNV=function(idnv,callback){
    models.thongbao
    .findAndCountAll({
        where:{idNguoiNhan:idnv}
    })
    .then(function(err){
        callback(err);
    })
}
controller.updateThongbao=function(idnotify){
    models.thongbao
    .update({
        DaXem:1
    },{
        where:{id:idnotify}
    })
    .then(function(err){
        callback(err);
    })
}
module.exports = controller;