var controller = {};
var models = require('../models');
controller.getAll=function (callback)
{
    models.lichsukiemke
    .findAll({raw:true})
    .then(function(lskk){
        callback(lskk);
    });
};
controller.getAllIncTB=function(callback){
    models.lichsukiemke
    .findAll({
    include:[{model:models.ThietBi,attributes:['TenThietBi','GiaTri']}],
    required:true,
    })
    .then(function(lskk){
        callback(lskk);
    });
}
controller.getByID=function(id,callback){
    models.lichsukiemke
    .findOne({
        where:{id:id}
    })
    .then(function(lskk){
        callback(lskk);
    })
}
controller.createLS=function(data,callback){
    models.lichsukiemke
    .create(data)
    .then(function(data){
        callback(data);
    })
    .catch(function(err){
        callback(err);
    });
}
controller.getCompareLS=function(data,callback){
    models.sequelize.query("SELECT a.MaThietBi,c.TenThietBi,a.TinhTrang,a.createdAt ,b.TinhTrang as 'Tinhtrang2',b.createdAt as 'Thoigian2' FROM demo.lichsukiemke as a inner join lichsukiemke as b on a.MaThietBi=b.MaThietBi left join demo.thietbis as c on a.MaThietBi=c.id where year(a.createdAt)=:nam1 and year(b.createdAt)=:nam2 group by MaThietBi;",{replacements:{nam1:data.nam1,nam2:data.nam2},type:models.Sequelize.QueryTypes.SELECT})
    .then(function(lskk){
        callback(lskk);
    })
    .catch(function(err){
        callback(err);
    });
}
module.exports = controller;