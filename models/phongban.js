'use strict';
module.exports = (sequelize, Sequelize) => {
  const phongban = sequelize.define('phongban', {
    TenPhong:Sequelize.STRING,
  }, {timestamps:false,});
  phongban.associate = function(models) {
    // associations can be defined here
    phongban.hasMany(models.bomon,{foreignKey:'IDPhongBan'});
    phongban.hasMany(models.TaiSan,{foreignKey:'MaPhong'});
    phongban.hasMany(models.Users,{foreignKey:'idPhongban'})
  };
  return phongban;
};