'use strict';
module.exports = (sequelize, Sequelize) => {
  const bomon = sequelize.define('bomon', {
    TenBoMon: Sequelize.STRING,
    IDPhongBan:Sequelize.INTEGER,
  }, {timestamps:false});
  bomon.associate = function(models) {
    // associations can be defined here
    bomon.belongsTo(models.phongban,{foreignKey:'IDPhongBan'});
  };
  return bomon;
};