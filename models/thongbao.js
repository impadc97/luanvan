'use strict';
module.exports = (sequelize, Sequelize) => {
  const thongbao = sequelize.define('thongbao', {
    NoiDung: Sequelize.STRING,
    idNguoiNhan:Sequelize.INTEGER,
    ThoiGianThongBao:Sequelize.DATE,
    DaXem:Sequelize.BOOLEAN,
  }, {timestamps:false,freezeTableName: true});
  thongbao.associate = function(models) {
    // associations can be defined here
  };
  return thongbao;
};