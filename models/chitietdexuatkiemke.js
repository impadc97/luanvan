'use strict';
module.exports = (sequelize, Sequelize) => {
  const chitietdexuatkiemke = sequelize.define('chitietdexuatkiemke', {
    dexuatkiemkeId:Sequelize.INTEGER,
    ThietBiId:Sequelize.INTEGER,
    TrangThaiTbTamThoi:Sequelize.STRING,
  }, {timestamps:false});
  chitietdexuatkiemke.associate = function(models) {
    // associations can be defined here
    chitietdexuatkiemke.belongsTo(models.ThietBi,{foreignkey:"ThietBiId", targetKey: "id"});
    chitietdexuatkiemke.belongsTo(models.dexuatkiemke,{foreignkey:"dexuatkiemkeId", targetKey: "id"});

  };
  return chitietdexuatkiemke;
};