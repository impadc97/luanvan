'use strict';
module.exports = (sequelize, DataTypes) => {
  var Users = sequelize.define('Users', {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    hoten: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    isActive: DataTypes.BOOLEAN,
    isAdmin: DataTypes.BOOLEAN,
    idPhongban: DataTypes.INTEGER,
    role: DataTypes.INTEGER
  }, {});
  Users.associate = function (models) {
    // associations can be defined here
    Users.belongsTo(models.phongban, { foreignKey: 'idPhongban' });
    Users.hasMany(models.dexuatkiemke, { as: "NguoiTao", foreignKey: 'IdNguoiTao', targerKey: 'IdNguoiTao' });
    Users.hasMany(models.dexuatkiemke, { as: "NguoiDuyet", foreignKey: 'IdNguoiDuyet', targerKey: 'IdNguoiDuyet' });
  };
  return Users;
};
