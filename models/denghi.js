'use strict';
module.exports = (sequelize, Sequelize) => {
  const denghi = sequelize.define('denghi', {
    NoiDungHoSo:Sequelize.STRING,
    SuDungKinhPhi:Sequelize.STRING,
    NgayDeNghi:Sequelize.DATE,
    NgayNhan:Sequelize.DATE,
    IdNguoiYeucau:Sequelize.INTEGER,
    idNguoiXulyDon:Sequelize.INTEGER,
    LoaiDeNghi:Sequelize.INTEGER,
    Tinhtrang:Sequelize.INTEGER,
    LyDoHuyDon:Sequelize.STRING,
    dsIdThietBiLienQUan:Sequelize.STRING,
    DonDenghiXetDuyetImgPath:Sequelize.STRING,
    DonDenghiFinalImgPath:Sequelize.STRING,
    DsBaogiaPath:Sequelize.STRING,
    BaogiaFinalPath:Sequelize.STRING,
    NgayThuHoi:Sequelize.DATE,
  }, {});
  denghi.associate = function(models) {
    // associations can be defined here
    //denghi.belongsTo(models.Users,{foreignkey:'IdNguoiYeucau'});
  };
  return denghi;
};