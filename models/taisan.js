'use strict';
module.exports = (sequelize, Sequelize) => {
  const TaiSan = sequelize.define('TaiSan', {
    TenTaiSan: Sequelize.STRING,
    DanhMuc:Sequelize.STRING,
    Model:Sequelize.STRING,
    NamSanXuat:Sequelize.INTEGER,
    XuatXu:Sequelize.STRING,
    SoLuong:Sequelize.INTEGER,
    GiaTri:Sequelize.INTEGER,
    MaDeNghi:Sequelize.INTEGER,
    ThoiHanBaoHanh:Sequelize.DATE,
    SLThanhLy:Sequelize.INTEGER,
    DonViSuDung:Sequelize.STRING,
    MaPhong:Sequelize.INTEGER
  }, {});
  TaiSan.associate = function(models) {
    // associations can be defined here
    TaiSan.hasMany(models.ThietBi,{foreignKey:'TS_MaTaiSan'},{onDelete: 'CASCADE'});
    TaiSan.belongsTo(models.phongban,{foreignKey:'MaPhong'});
  };
  return TaiSan;
};