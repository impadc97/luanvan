'use strict';
module.exports = (sequelize, Sequelize) => {
  const ThietBi = sequelize.define('ThietBi', {
    TenThietBi: Sequelize.STRING,
    TinhTrangThanhLy:Sequelize.BOOLEAN,
    NgayThanhLy:Sequelize.DATE,
    TS_MaTaiSan:Sequelize.INTEGER,
    NamSuDung:Sequelize.INTEGER,
    GiaTri:Sequelize.INTEGER,
    PhongBan:Sequelize.STRING,
    TinhTrang:Sequelize.STRING,
    MaDVSD:Sequelize.INTEGER,
    IdBomon: Sequelize.INTEGER,
  }, {});
  ThietBi.associate = function(models) {
    // associations can be defined here
    ThietBi.belongsTo(models.TaiSan,{foreignKey:'TS_MaTaiSan'});
    ThietBi.belongsTo(models.bomon,{foreignKey:'MaDVSD'});
    ThietBi.belongsTo(models.phongban,{foreignKey:'MaDVSD'});
    ThietBi.hasMany(models.chitietdexuatkiemke,{foreignKey:"ThietBiId", targetKey: "ThietBiId"});
  };
  return ThietBi;
};