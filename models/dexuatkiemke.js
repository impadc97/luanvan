'use strict';
module.exports = (sequelize, Sequelize) => {
  const dexuatkiemke = sequelize.define('dexuatkiemke', {
    IdNguoiTao: Sequelize.INTEGER,
    IdNguoiDuyet: Sequelize.INTEGER,
  }, {});
  dexuatkiemke.associate = function (models) {
    // associations can be defined here
    dexuatkiemke.belongsTo(models.Users, {as: "NguoiTao", foreignKey: 'IdNguoiTao', targetKey: 'id' });
    dexuatkiemke.belongsTo(models.Users, {as: "NguoiDuyet",  foreignKey: 'IdNguoiDuyet', targetKey: 'id' });
    dexuatkiemke.hasMany(models.chitietdexuatkiemke,{foreignKey:"dexuatkiemkeId", targetKey: "dexuatkiemkeId"});
  };
  return dexuatkiemke;
};