'use strict';
module.exports = (sequelize, DataTypes) => {
  const lichsukiemke = sequelize.define('lichsukiemke', {
    MaThietBi: DataTypes.INTEGER,
    TinhTrang:DataTypes.STRING,
  }, {freezeTableName: true});
  lichsukiemke.associate = function(models) {
    lichsukiemke.belongsTo(models.ThietBi,{foreignKey:'MaThietBi'}),
    lichsukiemke.belongsTo(models.lichsukiemke,{as:'db2',foreignKey:'MaThietBi'});
  };
  return lichsukiemke;
};