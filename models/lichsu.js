'use strict';
module.exports = (sequelize, Sequelize) => {
  const lichsu = sequelize.define('lichsu', {
    Loai:Sequelize.INTEGER,
    PhongBanCu:Sequelize.INTEGER,
    TS_MaTaiSan:Sequelize.INTEGER,
    MaDeNghi:Sequelize.INTEGER,
    GiaTri:Sequelize.INTEGER,
  }, {});
  lichsu.associate = function(models) {
    // associations can be defined here
    lichsu.belongsTo(models.TaiSan,{foreignKey:'TS_MaTaiSan'});
    lichsu.belongsTo(models.phongban,{foreignKey:'PhongBanCu'});
    lichsu.belongsTo(models.denghi,{foreignKey:'MaDeNghi'});
  };
  return lichsu;
};