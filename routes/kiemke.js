var express = require('express');
var router = express.Router();
var kiemkeController = require('../controllers/kiemkeController');

router.get('/', function (req, res) {
  kiemkeController.getAll(function (dexuatkiemkes) {
    res.render('qltb-dxkk', { dexuatkiemkes: dexuatkiemkes });
  })
});

router.post('/', function (req, res) {
  var tmp = {};

  tmp.IdNguoiTao = Number.parseInt(req.body.idnguoitao);
  tmp.IdNguoiDuyet = Number.parseInt(req.body.idnguoiduyet);

  kiemkeController.createDXKK(tmp, function (err) {
    if (err) res.sendStatus(500);
    else {
      res.sendStatus(200);
      res.end();
    }
  });
})

module.exports = router;
