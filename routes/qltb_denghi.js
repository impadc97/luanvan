var express = require('express');
var router = express.Router();
var path = require("path");
let multer = require("multer");
var denghiController = require('../controllers/denghiController');
// router.get('/',function(req,res){
//     denghiController.getAll(function(denghi){
//         console.log(denghi)
//         res.render('qltb-denghi', { title: 'DS De nghi',denghi:denghi });
//     });
// })

// Khởi tạo biến cấu hình cho việc lưu trữ file upload
let diskStorage = multer.diskStorage({
    destination: (req, file, callback) => {
        // Định nghĩa nơi file upload sẽ được lưu lại
        callback(null, "./public/assests/doc/baogia/");
    },
    filename: (req, file, callback) => {
        // ở đây các bạn có thể làm bất kỳ điều gì với cái file nhé.
        // Mình ví dụ chỉ cho phép tải lên các loại ảnh png & jpg
        //   let math = ["image/png", "image/jpeg"];
        //   if (math.indexOf(file.mimetype) === -1) {
        //     let errorMess = `The file <strong>${file.originalname}</strong> is invalid. Only allowed to upload image jpeg or png.`;
        //     return callback(errorMess, null);
        //   }

        // Tên của file thì mình nối thêm một cái nhãn thời gian để đảm bảo không bị trùng.
        let filename = `${Date.now()}-dsBaogia-${file.originalname}`;
        callback(null, filename);
    }
});

// Khởi tạo middleware uploadFile với cấu hình như ở trên,
// Bên trong hàm .single() truyền vào name của thẻ input, ở đây là "file"
var uploaddsBaogia = multer({ storage: diskStorage });

router.get('/', function (req, res) {

    //Kiểm tra session timeout chưa
    if (req.session.user != null) {
        let idNhanvienQltb = req.session.user.id;

        denghiController.getAllforQTLB(idNhanvienQltb, function (data) {
            //console.log(data)
            res.render('qltb-denghi', { title: 'DS De nghi', data: data });
        });
    }
    else {
        res.redirect("/users");
    }
})

router.get('/denghis',function(req, res){
    denghiController.getAll(function(denghis){
        res.render('admin-denghi', { title: 'DS De nghi', denghis: denghis });
    });
})

router.get('/:id', function (req, res) {
    denghiController.getDetail(req.params.id, function (detail) {
        denghiController.getBoMon(detail.nguoidedon.idPhongban,function(bomon){
            if(detail.denghi.LoaiDeNghi == 1){
                res.render('qltb-denghi-detail', {
                    title: detail.denghi.id,
                    data: detail,
                    bomon:bomon,
                });
            }
            else if (detail.denghi.LoaiDeNghi==2){
                res.render('qltb-denghi-suachua-detail', {
                    title: detail.denghi.id,
                    data: detail,
                    bomon:bomon,
                });
            }
            else if (detail.denghi.LoaiDeNghi==3){
                res.render('qltb-denghi-detail-thanhly',{
                    title:detail.denghi.id,
                    data:detail,
                    bomon:bomon,
                })
            }
        })
    })
});

router.get('/admin/:id', function(req, res){
    denghiController.getDetail(req.params.id, function (detail) {
        denghiController.getBoMon(detail.nguoidedon.idPhongban,function(bomon){
            res.render('admin-denghi-detail', {
                title: detail.denghi.id,
                data: detail,
                bomon:bomon,
            });
        })
    })
});

router.get('/json/:id', (req, res) => {
    denghiController.getByID(req.params.id, (denghi) =>{
        res.send(denghi);
    })
})

router.post('/', function (req, res) {
    var tmp = {};
    tmp.id = req.body.id;
    tmp.NoiDungHoSo = req.body.noidung;
    if (req.body.ngaynhandenghi != null) {
        tmp.NgayNhanDeNghi = req.body.ngaynhandenghi;
    }
    if (req.body.ngaydenghi != null) {
        tmp.NgayDeNghi = req.body.ngaydenghi;
    }
    if (req.body.namthuchien != null) {
        tmp.NamThucHien = req.body.namthuchien;
    }
    if (req.body.loaidenghi != null) {
        tmp.LoaiDeNghi = req.body.loaidenghi;
    }
    denghiController.createDeNghi(tmp, function (err) {
        if (err) res.sendStatus(500);
        else {
            res.sendStatus(200);
            res.end();
        }
    });
});
router.put('/', function (req, res) {
    var editContent = {};
    editContent.id = req.body.id;
    editContent.NoiDungHoSo = req.body.noidung;
    editContent.NgayDeNghi = req.body.ngaydenghi;
    editContent.NgayNhanDeNghi = req.body.ngaynhandenghi;
    editContent.NamThucHien = req.body.namthuchien;
    editContent.DaThucHien = req.body.dathuchien;
    denghiController.updateDeNghi(editContent, function () {
        res.sendStatus(200);
        res.end();
    })
})

router.patch('/duyetdon', function (req, res) {

    var idDenghi = req.body.id;
    var denghiCanduyet = {};

    denghiController.getByID(idDenghi, function (denghi) {
        denghiCanduyet = denghi;
        console.log(idDenghi);
    });

    //Kiểm tra session timeout chưa và đơn đã có ai nhận chưa
    if (req.session.user != null && denghiCanduyet.idNhanvienQltb == null) {
        let idNhanvienQltb = req.session.user.id;
        denghiController.updateNguoixulyDon(idNhanvienQltb, idDenghi, function (data) {
            //console.log(data)
            res.send(data);
        });
    }
    else {
        res.redirect("/users");
    }
})

router.patch('/nextstep', function (req, res) {
    let idDenghi = req.body.id;
    let stepcontent = {};
    stepcontent.curStatus = parseInt(req.body.curstatus);
    switch(stepcontent.curStatus){
        case 2:{
            stepcontent.DonDenghiFinalImgPath = req.body.fileanhfinal;
        }break;
    }
    denghiController.nextStep(idDenghi, stepcontent, function (rs) {
        res.send(rs);
    });
})
router.patch('/nextsteptly',function(req,res){
    var idDenghi = req.body.id;
    var curstatus = parseInt(req.body.curstatus);
    var ngaythuhoi=req.body.NgayThuHoi;
    var signal=req.body.signal;
    denghiController.getByID(idDenghi,function(denghi){
        if(denghi.LoaiDeNghi==3 && curstatus==3) // don thanh ly va ngay thu hoi
        {
            if(signal==0) // qttb gui
            {
                denghiController.updateNgayThuHoi(idDenghi,ngaythuhoi,function(rs){
                    res.send(rs);
                })
            }
            else 
            {
                denghiController.nextStepTly(idDenghi,curstatus,function(rs){
                    res.send(rs);
                });
            }
        }
        else {
            denghiController.nextStepTly(idDenghi,curstatus,function(rs){
                res.send(rs);
            });
        }
    })
})
router.patch('/huydon', function (req, res) {

    var idDenghi = req.body.id;
    var lydohuydon = req.body.lydohuydon;

    denghiController.huydon(idDenghi,lydohuydon,function(rs){
        res.send(rs);
    });
})

router.patch('/luudsbaogia', function (req, res) {

    var idDenghi = req.body.id;
    var DsBaogiaPath = req.body.dsbaogiapath;

    denghiController.luudsbaogia(idDenghi,DsBaogiaPath,function(rs){
        res.send(rs);
    });
})

router.post('/uploadsdsbaogia', uploaddsBaogia.array('baogias',3), function (req, res) {
    // Thực hiện upload file, truyền vào 2 biến req và res
    console.log(`------Request body-----`);
    console.log(req.body);

    console.log(`------Request file-----`);
    console.log(req.files);

    console.log(`------Test Done-----`)

    // Không có lỗi thì lại render cái file ảnh về cho client.
    // Đồng thời file đã được lưu vào thư mục uploads
    //console.log(path.join(`${__dirname}/uploads/${req.file.filename}`));
    res.status(200).send(req.files);
});

module.exports = router;