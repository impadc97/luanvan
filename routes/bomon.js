var express = require('express');
var router = express.Router();
var bomonController = require('../controllers/bomonController');

router.get('/phongban/curuser/', function (req, res) {
    //Kiểm tra session timeout chưa
    if (req.session.user != null) {
        bomonController.getByIdPhongBan(req.session.user.idPhongban, function(bomons){
            res.send(bomons);
        })
    }
    else {
        res.redirect("/users");
    }

})

router.get('/raw', function (req, res){
    bomonController.getAll(function(bomons){
        res.send(bomons);
    })
})

module.exports = router;