var express = require('express');
var router = express.Router();
var usersController = require('../controllers/userController.js');

router.get('/',function(req,res){
    res.redirect('/admin/users');
})

router.get('/users',function(req,res){

    usersController.getAll(function(data){
        res.render('admin-user',{
            title:'QLND',
            data:data
        });
    })

    // phongbanController.getAll(function(phongban){
    //     res.render('admin-user',{
    //         title:'QLND',
    //         phongban:phongban
    //     });
    // });
})

router.put('/users',function(req,res)
{
    var editContent={};
            editContent.id=req.body.id;
            editContent.username=req.body.username;
            editContent.password=req.body.password;
            editContent.hoten=req.body.hoten;
            editContent.phone=req.body.phone;
            editContent.idPhongban=req.body.idPhongban;
            editContent.email=req.body.email;
            editContent.isActive=req.body.isActive;
            editContent.isAdmin=req.body.isAdmin;
            editContent.role=req.body.role;
            
    usersController.updateUser(editContent,function(){
        res.sendStatus(200);
        res.end();
    })
})

module.exports=router;