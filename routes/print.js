var express = require('express');
var router = express.Router();
var denghiController = require('../controllers/denghiController');

router.get('/mauso/:id',( req, res) => {
    var so = parseInt(req.params.id);
    res.render('mauso',{layout: 'print_layout.hbs', so:so});
    
});

module.exports = router;