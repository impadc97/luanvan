var express = require('express');
var router = express.Router();
var lskkController = require('../controllers/lskiemkeController');

router.get('/sosanh/:nam1&:nam2',function(req,res){
    var data={};
    if(req.params.nam1 === undefined)
    {
        data.nam1=2019;
        data.nam2=2018;
        
    }
    else {
        data.nam1=req.params.nam1;
        data.nam2=req.params.nam2;
    }
    lskkController.getCompareLS(data,function(lichsu){
        res.render('SoSanhTinhTrang',{title:'So sánh tình trạng',lichsu:lichsu,data:data})
        //res.send(lichsu);
    })
})
router.get('/',function(req,res){
    lskkController.getAllIncTB(function(lichsu){
         res.render('LichSuTinhTrang',{title:'Lịch sử tình trạng',lichsu:lichsu});
    })
})
router.post('/',function(req,res){
    data={};
    data.MaThietBi=req.body.id;
    data.TinhTrang=req.body.TinhTrang;
    lskkController.createLS(data,function(err){
            res.sendStatus(200);
            res.end();
    })
})

router.get('/raw', function(req,res){
    lskkController.getAll(function(lichsus){
        res.send(lichsus);
        console.log(lichsus);
    })
})

module.exports = router;