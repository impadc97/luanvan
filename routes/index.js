var express = require('express');
var router = express.Router();
var thongbaoController = require('../controllers/thongbaoController');
var lskiemkeController = require('../controllers/lskiemkeController');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect('/users');
});
router.get('/navbar',function(req,res){
  if (req.session.user != null){
    thongbaoController.getByIDNV(req.session.user.id,function(thongbao){
      res.render('partials/navbar',{layout:false,thongbao:thongbao})
    })
  }
})
router.post('/navbar',function(req,res){
  let content={}
  content.idNguoiNhan=req.body.IdNguoiYeucau;
  content.NoiDung=req.body.NoiDungThongBao;
  content.DaXem=0;
  thongbaoController.createThongbao(content,function(err){
    if (err) res.sendStatus(500);
    else {
        res.sendStatus(200);
        res.end();
    }
  })
})

module.exports = router;
