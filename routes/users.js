var express = require('express');
var usersController = require('../controllers/userController.js');
var passport = require('passport');
let LocalStrategy = require('passport-local').Strategy;
var router = express.Router();

/* GET users listing. */
router.get('/', isLoggedIn, function (req, res) {
	res.locals.error = req.flash('error_message');
	res.render('login')
});
router.post('/', passport.authenticate('local', {
	failureRedirect: '/users', failureFlash: true
}),
	function (req, res) {
		req.flash('success_message', 'Đăng nhập thành công!');		
		//res.redirect('/thietbi');

		usersController.getUserByUsername(req.body.username, function (err, user) {
			let role = user.role;
			req.session.user = user;

			switch(role){
				case 0 : {
					res.redirect('users/logout');
				} break;
				case 1 : res.redirect('/thietbisohuu'); break;
				case 2 : res.redirect('/thietbi'); break;
				case 3: res.redirect('/admin');break;
			}
		});
	}
);
passport.use(new LocalStrategy({
	usernameField: 'username',
	passwordField: 'password',
	passReqToCallback: true
},
	function (req, username, password, done) {
		usersController.getUserByUsername(username, function (err, user) {
			if (err) { return done(err); }
			if (!user || user.isActive == false) {
				return done(null, false, req.flash('error_message', 'No username is found'));
			}
			usersController.comparePassword(password, user.password, function (err, isMatch) {
				if (err) { return done(err); }
				if (isMatch) {
					userlogin = username;
					idlogin = user.id;
					return done(null, user, req.flash('success_message', 'You have successfully logged in!!'));
				}
				else {
					return done(null, false, req.flash('error_message', 'Incorrect Password'));
				}
			});
		});
	}
));
passport.serializeUser(function (user, done) {
	done(null, user.id);
});

passport.deserializeUser(function (id, done) {
	usersController.getUserById(id, function (err, user) {
		done(err, user);
	});
});
router.get('/logout', async function (req, res) {
	await req.logout();
	// req.flash('success_message', 'You are logged out');
	req.session.user = null;

	return res.redirect('/users');
});
router.get('/register', function (req, res) {
	res.render('register');
})
router.post('/register', function (req, res) {
	let data = {};
	
	data.username = req.body.username;
	data.password = req.body.password;
	data.hoten = req.body.hoten;
	data.isActive = true;
	data.isAdmin = false;
	data.phone = req.body.phone;
	data.email = req.body.email;
	data.role=req.body.role;
    data.idPhongban=req.body.idPhongban;

	
	usersController.createUser(data, function (err) {
		if (err) throw err;
		console.log(data);
	});
	req.flash('success_message', 'You have registered, Now please login');
	res.redirect('/users');
})
function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
		res.redirect('/')
	}
	else {
		next();
	}
}
function isAdmin(req, res, next) {
	if (req.isAuthenticated() && req.user.isAdmin == true) {
		res.redirect('/admin')
	}
	else {
		next();
	}
}

module.exports = router;
