var express = require('express');
var router = express.Router();
var lichsuController = require('../controllers/lichsuController');
var taisanController=require('../controllers/taisanController');
router.get('/',function(req,res){
    lichsuController.getFull(function(lichsu){
        res.render('quanlytanggiam',{lichsu:lichsu});
        // res.send(lichsu);
        // res.end();
    })
})
router.post('/',function(req,res){
    data={};
    data.Loai=req.body.Loai;
    data.PhongBanCu=req.body.PhongBanCu;
    data.TS_MaTaiSan=req.body.TS_MaTaiSan;
    data.GiaTri=req.body.GiaTri;
    data.MaDeNghi=req.body.MaDeNghi;
    if(data.Loai==2)
    {
        var tangts={};
        var giamts={};
        tangts.Loai=1;
        tangts.TS_MaTaiSan=req.body.TS_MaTaiSan;
        tangts.GiaTri=req.body.GiaTri;
        lichsuController.createLichsu(tangts,function(){})
        taisanController.getById(req.body.MaTaiSanCu,function(rests){
            giamts.Loai=2;
            giamts.TS_MaTaiSan=req.body.TS_MaTaiSan;
            giamts.GiaTri=req.body.GiaTri;
            giamts.PhongBanCu=rests.MaPhong;
            lichsuController.createLichsu(giamts,function(){
                res.sendStatus(200);
                res.end();
            });
        })
    }
    else 
    {
        lichsuController.createLichsu(data,function(err){
            res.sendStatus(200);
            res.end();
        })
    }
})
module.exports=router;