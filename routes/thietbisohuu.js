var express = require('express');
var router = express.Router();
var thietbiController=require('../controllers/thietbiController');
var lichsuController=require('../controllers/lichsuController');
var taisanController=require('../controllers/taisanController');
router.get('/',function(req,res)
{
    //Kiểm tra session timeout chưa
    if(req.session.user != null){
        console.log(`-----------USER--------------\n`);
        console.log(req.session.user);
        
        //Id phòng ban của trưởng phòng
        let idPhongban = parseInt(req.session.user.idPhongban);
    
        thietbiController.getByIdPhongbanV2(idPhongban,function(thietbi){
            res.render('thietbisohuu',{
                title:'Thiet bi',
                thietbi:thietbi
            })
        })
    }
    else{
        res.redirect("/users");
    }
})

router.get('/json', function(req, res){
    //Kiểm tra session timeout chưa
    if(req.session.user != null){
        console.log(`-----------USER--------------\n`);
        console.log(req.session.user);
        
        //Id phòng ban của trưởng phòng
        let idPhongban = parseInt(req.session.user.idPhongban);
    
        thietbiController.getByIdPhongbanV2(idPhongban,function(thietbi){
            res.send(thietbi);
        })
    }
    else{
        res.redirect("/users");
    }
})

router.get('/raw',function(req,res){
        thietbiController.getAll(function(thietbi){
            res.send(thietbi)
        })
    })
router.get('/:id',function(req,res)
{
    thietbiController.getById(req.params.id,function(thietbi){
        res.render('dvsd-thietbi-detail',{
            title:thietbi.TenThietBi,
            thietbi:thietbi
        });
    })
})
router.put('/',function(req,res)
{
    var data={};
    data.id=req.body.id;
    data.TenThietBi=req.body.TenThietBi;
    data.TinhTrangThanhLy=req.body.TinhTrangThanhLy
    data.NgayThanhLy=req.body.NgayThanhLy;
    data.NamSuDung=req.body.NamSuDung;
    data.GiaTri=req.body.GiaTri;
    data.PhongBan=req.body.PhongBan;
    data.TinhTrang=req.body.TinhTrang;
    thietbiController.updateThietbi(data,function(){
        res.sendStatus(200);
        res.end();
    })
})
router.put('/:id',function(req,res){
    var data={};
    data.id=req.body.id;
    data.TinhTrang=req.body.TinhTrang;
    thietbiController.updateTTThietbi(data,function(){
        res.sendStatus(200);
    })
})
router.delete('/',function(req,res){
    var data={};
    data.id=req.body.id;
    thietbiController.deleteThietBi(data.id,function(){
        res.sendStatus(200);
    })
})
router.post('/lichsu',function(req,res)
{
    var data={};
    data.id=req.body.id;
    thietbiController.getDVSD(data.id,function(taisan){
        var lichsu={};
        lichsu.DonViSuDung=taisan.TaiSan.DonViSuDung;
        lichsu.TS_MaThietBi=data.id;
        lichsu.loai="Giam"
        lichsuController.createLichsu(lichsu,function(err){
            if(err) throw err.status;
        });
    })
    res.sendStatus(200);
});
router.post('/thanhly',function(req,res){
    var data={};
    data.id=req.body.id;
    thietbiController.ThanhLyThietbi(data,function(){
        res.sendStatus(200);
    })
})
router.post('/',function(req,res){
    console.log(req.body.array);
    thietbiController.createMultiThietBi(req.body.array,function(err){
        if(err) res.sendStatus(500);
        else res.sendStatus(200); 
    })
    
})


module.exports=router;