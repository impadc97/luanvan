var express = require('express');
var router = express.Router();
var path = require("path");
let multer = require("multer");
var denghiController = require('../controllers/denghiController');
var phongbanController = require('../controllers/phongbanController');
var thietbiController = require('../controllers/thietbiController');

// Khởi tạo biến cấu hình cho việc lưu trữ file upload
let diskStorage = multer.diskStorage({
    destination: (req, file, callback) => {
        // Định nghĩa nơi file upload sẽ được lưu lại
        callback(null, "./public/assests/img/dondenghi/banchoxetduyet");
    },
    filename: (req, file, callback) => {
        // ở đây các bạn có thể làm bất kỳ điều gì với cái file nhé.
        // Mình ví dụ chỉ cho phép tải lên các loại ảnh png & jpg
        //   let math = ["image/png", "image/jpeg"];
        //   if (math.indexOf(file.mimetype) === -1) {
        //     let errorMess = `The file <strong>${file.originalname}</strong> is invalid. Only allowed to upload image jpeg or png.`;
        //     return callback(errorMess, null);
        //   }

        // Tên của file thì mình nối thêm một cái nhãn thời gian để đảm bảo không bị trùng.
        let filename = `${Date.now()}-dondenghi-${file.originalname}`;
        callback(null, filename);
    }
});

let diskStorageFinal = multer.diskStorage({
    destination: (req, file, callback) => {
        // Định nghĩa nơi file upload sẽ được lưu lại
        callback(null, "./public/assests/img/dondenghi/final");
    },
    filename: (req, file, callback) => {
        // ở đây các bạn có thể làm bất kỳ điều gì với cái file nhé.
        // Mình ví dụ chỉ cho phép tải lên các loại ảnh png & jpg
        //   let math = ["image/png", "image/jpeg"];
        //   if (math.indexOf(file.mimetype) === -1) {
        //     let errorMess = `The file <strong>${file.originalname}</strong> is invalid. Only allowed to upload image jpeg or png.`;
        //     return callback(errorMess, null);
        //   }

        // Tên của file thì mình nối thêm một cái nhãn thời gian để đảm bảo không bị trùng.
        let filename = `${Date.now()}-dondenghi-${file.originalname}`;
        callback(null, filename);
    }
});

// Khởi tạo middleware uploadFile với cấu hình như ở trên,
// Bên trong hàm .single() truyền vào name của thẻ input, ở đây là "file"
var uploadFile = multer({ storage: diskStorage });

var uploadFileFinal = multer({ storage: diskStorageFinal });

router.get('/', function (req, res) {
    //Kiểm tra session timeout chưa
    if (req.session.user != null) {
        console.log(`-----------USER--------------\n`);
        console.log(req.session.user);

        var $ownerId = req.session.user.id;
        console.log($ownerId);
        denghiController.getByOwnerID($ownerId, function (denghi) {
            var data = {
                denghi: denghi,
                curUser: $ownerId
            }
            res.render('denghisohuu', { title: 'DS De nghi', data: data });
        });
    }
    else {
        res.redirect("/users");
    }

})

router.post('/', function (req, res) {
    var tmp = {};
    tmp.id = req.body.id;
    tmp.NoiDungHoSo = req.body.noidung;
    if (req.body.ngaynhandenghi != null) {
        tmp.NgayNhanDeNghi = req.body.ngaynhandenghi;
    }
    if (req.body.ngaydenghi != null) {
        tmp.NgayDeNghi = req.body.ngaydenghi;
    }
    if (req.body.namthuchien != null) {
        tmp.NamThucHien = req.body.namthuchien;
    }
    if (req.body.loaidenghi != null) {
        tmp.LoaiDeNghi = req.body.loaidenghi;
    }
    denghiController.createDeNghi(tmp, function (err) {
        if (err) res.sendStatus(500);
        else {
            res.sendStatus(200);
            res.end();
        }
    });
});
router.put('/', function (req, res) {
    var editContent = {};
    editContent.id = req.body.id;
    editContent.NoiDungHoSo = req.body.noidung;
    editContent.NgayDeNghi = req.body.ngaydenghi;
    editContent.NgayNhanDeNghi = req.body.ngaynhandenghi;
    editContent.NamThucHien = req.body.namthuchien;
    editContent.DaThucHien = req.body.dathuchien;
    denghiController.updateDeNghi(editContent, function () {
        res.sendStatus(200);
        res.end();
    })
})

router.get('/add', function (req, res) {
    //Kiểm tra session timeout chưa
    if (req.session.user != null) {

        var curUser = req.session.user;
        phongbanController.getById(req.session.user.idPhongban, function (phongban) {
            thietbiController.getByIdPhongbanV2(req.session.user.idPhongban, function (thietbi) {
                res.render('denghisohuu-add', { title: 'DS De nghi', curUser: curUser, phongban: phongban, thietbi: thietbi });
            })
        })
    }
    else {
        res.redirect("/users");
    }
})


router.post('/add/uploads', uploadFile.single('file'), function (req, res) {
    // Thực hiện upload file, truyền vào 2 biến req và res
    console.log(`------Request body-----`);
    console.log(req.body);

    console.log(`------Request file-----`);
    console.log(req.file);

    console.log(`------Test Done-----`)

    // Không có lỗi thì lại render cái file ảnh về cho client.
    // Đồng thời file đã được lưu vào thư mục uploads
    console.log(path.join(`${__dirname}/uploads/${req.file.filename}`));
    res.status(200).send(req.file.filename);
});

router.post('/add/uploadsfinal', uploadFileFinal.single('file'), function (req, res) {
    // Thực hiện upload file, truyền vào 2 biến req và res
    console.log(`------Request body-----`);
    console.log(req.body);

    console.log(`------Request file-----`);
    console.log(req.file);

    console.log(`------Test Done-----`)

    // Không có lỗi thì lại render cái file ảnh về cho client.
    // Đồng thời file đã được lưu vào thư mục uploads
    console.log(path.join(`${__dirname}/uploads/${req.file.filename}`));
    res.status(200).send(req.file.filename);
});

router.post('/add', function (req, res) {
    var data = {};
    data.NoiDungHoSo = req.body.datadondenghi.noidunghoso;
    data.NgayDeNghi = req.body.datadondenghi.ngaydenghi;
    data.LoaiDeNghi = req.body.datadondenghi.loaidenghi;
    data.SuDungKinhPhi = req.body.datadondenghi.sdkp;
    data.Tinhtrang = req.body.datadondenghi.tinhtrang;
    data.DonDenghiXetDuyetImgPath = req.body.datadondenghi.donDdnghixetduyetimgpath;
    data.IdNguoiYeucau = req.session.user.id;
    data.dsIdThietBiLienQUan = req.body.datadondenghi.dsidthietbicansua;

    // tmp.id=req.body.id;
    // tmp.NoiDungHoSo=req.body.noidung;
    // if(req.body.ngaynhandenghi!=null){
    //     tmp.NgayNhanDeNghi=req.body.ngaynhandenghi;
    // }
    // if(req.body.ngaydenghi!=null){
    //     tmp.NgayDeNghi=req.body.ngaydenghi;
    // }
    // if(req.body.namthuchien!=null){
    //     tmp.NamThucHien=req.body.namthuchien;
    // }
    // if(req.body.loaidenghi!=null){
    //     tmp.LoaiDeNghi=req.body.loaidenghi;
    // }

    denghiController.createDeNghi(data, function (err) {
        if (err) res.sendStatus(500);
        else {
            res.sendStatus(200);
            res.end();
        }
    });
});

router.get('/export', function (req, res) {
    res.download('./output.docx', 'output.docx');
})
router.post('/export',function(req,res){
    phongbanController.getById(req.session.user.idPhongban,function(phongban){
        var data = {};
        data.NoiDungHoSo = req.body.noidung;
        data.NgayDeNghi = req.body.ngaydenghi;
        data.NguoiLienHe = req.body.nguoilienhe;
        data.SDT = req.body.sdt;
        data.NguonKinhPhi = req.body.nguonkinhphi;
        data.IdNguoiYeucau = req.session.user.id;
        data.DonVi=req.body.donvi;
        var ngaydenghi=new Date(req.body.ngaydenghi);
        data.d=ngaydenghi.getDate();
        data.m=ngaydenghi.getMonth()+1;
        data.y=ngaydenghi.getFullYear();
        data.thietbi=new Array;
        data.thietbi=[];
        var madon=parseInt(req.body.loaidon);
        var async=require('async');
        var ngaydenghi = new Date(req.body.ngaydenghi);
        data.d = ngaydenghi.getDate();
        data.m = ngaydenghi.getMonth() + 1;
        data.y = ngaydenghi.getFullYear();
        var madon = parseInt(req.body.loaidon);
        console.log(data);
        var PizZip = require('pizzip');
        var Docxtemplater = require('docxtemplater');
        var fs = require('fs');
        var path = require('path');
        content = fs
        var content = fs
            .readFileSync(path.resolve(__dirname, '../' + 'public/assests/doc/', 'DonDeNghi_Temp.docx'), 'binary');
        if(madon==3) content=fs
        .readFileSync(path.resolve(__dirname,'../'+'public/assests/doc/', 'DonThanhLy_Temp.docx'), 'binary');
        var zip = new PizZip(content);
        var doc = new Docxtemplater();
        doc.loadZip(zip);
        if(madon!=3){
            if (madon ==1) data.LoaiDon = 'mua mới';
            else if(madon==2) data.LoaiDon='sửa chữa';
            else if (madon==4) data.LoaiDon='bảo trì';
            doc.setData({   // Use {tagName} to replace variables
            donvi: data.DonVi,
            noi_dung: data.NoiDungHoSo,
            nguoi_lien_he: data.NguoiLienHe,
            dien_thoai: data.SDT,
            su_dung_kinh_phi: data.NguonKinhPhi,
            nguoi_de_nghi: data.NguoiDeNghi,
            loai: data.LoaiDon,
            d: data.d,
            m: data.m,
            y: data.y,
            noi_dung:data.NoiDungHoSo,
            nguoi_lien_he:data.NguoiLienHe,
            dien_thoai:data.SDT,
            su_dung_kinh_phi:data.NguonKinhPhi,
            nguoi_de_nghi:req.session.user.hoten,
            loai:data.LoaiDon,
            d:data.d,
            m:data.m,
            y:data.y,
            thietbi:data.thietbi,
            })
            try {
                doc.render()
            }
            catch (error) {
                var e = {
                    message: error.message,
                    name: error.name,
                    stack: error.stack,
                    properties: error.properties,
                }
                console.log(JSON.stringify({error: e}));
                // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
                throw error;
            }
            var buf = doc.getZip()
                        .generate({type: 'nodebuffer'});
            // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
            fs.writeFileSync('output.docx', buf);
            res.sendStatus(200);
            res.end();
        }
        else if (madon==3){
            data.LoaiDon = 'thanh lý';
            async.forEachOf(req.body.dstb,function(value,key,callback){
                thietbiController.getById(value,function(thietbi){
                    console.log(thietbi.TS_MaTaiSan);
                    var tb={};
                    tb.id=thietbi.id;
                    tb.STT=key+1;
                    tb.TenThietBi=thietbi.TenThietBi;
                    tb.NamSuDung=thietbi.NamSuDung;
                    tb.TS_MaTaiSan=thietbi.TS_MaTaiSan;
                    data.thietbi.push(tb);
                    callback();
                })
            },function(err){
                if (err) console.error(err.message);
                doc.setData({   // Use {tagName} to replace variables
                    donvi:phongban.TenPhong,
                    noi_dung:data.NoiDungHoSo,
                    nguoi_lien_he:data.NguoiLienHe,
                    dien_thoai:data.SDT,
                    su_dung_kinh_phi:data.NguonKinhPhi,
                    nguoi_de_nghi:req.session.user.hoten,
                    loai:data.LoaiDon,
                    d:data.d,
                    m:data.m,
                    y:data.y,
                    thietbi:data.thietbi,
                });
                try {
                    doc.render()
                }
                catch (error) {
                    var e = {
                        message: error.message,
                        name: error.name,
                        stack: error.stack,
                        properties: error.properties,
                    }
                    console.log(JSON.stringify({error: e}));
                    // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
                    throw error;
                }
                var buf = doc.getZip()
                            .generate({type: 'nodebuffer'});
                // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
                fs.writeFileSync('output.docx', buf);
                res.sendStatus(200);
                res.end();
            })
        }
    })
})
router.get('/:id', (req, res) => {
    denghiController.getDetail(req.params.id, function (data) {
        //console.log(data);
        if (data.denghi.Tinhtrang != -1) {
            if (data.denghi.LoaiDeNghi ==1) {
                res.render('dvsd-denghi-detail', {
                    title: data.denghi.id,
                    data: data
                });
            }
            else if (data.denghi.LoaiDeNghi ==2){
                res.render('dvsd-denghi-suachua-detail', {
                    title: data.denghi.id,
                    data: data
                });
            }
            else if(data.denghi.LoaiDeNghi ==3){
                res.render('dvsd-denghi-thanhly-detail', {
                    title: data.denghi.id,
                    data: data
                });
            }
        }
        else {

            res.render('denghisohuu-draft', {
                title: data.denghi.id,
                data: data
            })
        }
    })
})

router.patch('/nextstep', function (req, res) {

    let idDenghi = req.body.id;
    let stepcontent = {};
    console.log(req.body);
    stepcontent.curStatus = parseInt(req.body.curstatus);
    denghiController.nextStep(idDenghi, stepcontent, function (rs) {
        res.send(rs);
    });
})

router.patch('/chonbaogia', function(req, res){
    let idDenghi = req.body.id;
    let BaogiaFinalPath = req.body.selected;
    console.log(req.body);
    denghiController.chonBaogia(idDenghi, BaogiaFinalPath, function (rs) {
        res.send(rs);
    });
})

router.patch('/huydon', function (req, res) {

    var idDenghi = req.body.id;

    denghiController.huydon(idDenghi, function (rs) {
        res.send(rs);
    });
})
router.put('/huylich',function(req,res){
    var idDenghi=req.body.madon;
    console.log(idDenghi);
    // denghiController.huylich(idDenghi,function(rs){
    //     res.send(rs);
    // })
})
// router.post('/addsuachua', function (req, res) {
//     var data = {};
//     data.NoiDungHoSo = req.body.datadondenghi.noidunghoso;
//     data.NgayDeNghi = req.body.datadondenghi.ngaydenghi;
//     data.LoaiDeNghi = req.body.datadondenghi.loaidenghi;
//     data.SuDungKinhPhi = req.body.datadondenghi.sdkp;
//     data.Tinhtrang = req.body.datadondenghi.tinhtrang;
//     data.IdNguoiYeucau = req.session.user.id;

//     var idthietbicansua = req.body.idthietbicansua;

//     // tmp.id=req.body.id;
//     // tmp.NoiDungHoSo=req.body.noidung;
//     // if(req.body.ngaynhandenghi!=null){
//     //     tmp.NgayNhanDeNghi=req.body.ngaynhandenghi;
//     // }
//     // if(req.body.ngaydenghi!=null){
//     //     tmp.NgayDeNghi=req.body.ngaydenghi;
//     // }
//     // if(req.body.namthuchien!=null){
//     //     tmp.NamThucHien=req.body.namthuchien;
//     // }
//     // if(req.body.loaidenghi!=null){
//     //     tmp.LoaiDeNghi=req.body.loaidenghi;
//     // }

//     denghiController.createDeNghisuachua(data,idthietbicansua, function (rs) {
//         res.sendStatus(rs);
//         res.end();
//     });
// });

module.exports = router;