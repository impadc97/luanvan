var express = require('express');
var router = express.Router();
var thietbiController=require('../controllers/thietbiController');
var lichsuController=require('../controllers/lichsuController');
var taisanController=require('../controllers/taisanController');
var denghiController=require('../controllers/denghiController');
router.get('/',function(req,res)
{
    thietbiController.getAll(function(thietbi){
        res.render('thietbi',{
            title:'Thiet bi',
            thietbi:thietbi
        })
    })
})
router.get('/raw',function(req,res){
        thietbiController.getAll(function(thietbi){
            res.send(thietbi)
        })
    })
router.get('/:id',function(req,res)
{
    thietbiController.getById(req.params.id,function(thietbi){
        res.render('detailthietbi',{
            title:thietbi.TenThietBi,
            thietbi:thietbi
        });
    })
})
router.put('/',function(req,res)
{
    var data={};
    data.id=req.body.id;
    data.TenThietBi=req.body.TenThietBi;
    data.TinhTrangThanhLy=req.body.TinhTrangThanhLy
    data.NgayThanhLy=req.body.NgayThanhLy;
    data.NamSuDung=req.body.NamSuDung;
    data.GiaTri=req.body.GiaTri;
    data.PhongBan=req.body.PhongBan;
    data.TinhTrang=req.body.TinhTrang;
    thietbiController.updateThietbi(data,function(){
        res.sendStatus(200);
        res.end();
    })
})
router.put('/:id',function(req,res){
    var data={};
    data.id=req.body.id;
    data.TinhTrang=req.body.TinhTrang;
    thietbiController.updateTTThietbi(data,function(){
        res.sendStatus(200);
    })
})
router.delete('/',function(req,res){
    var data={};
    data.id=req.body.id;
    thietbiController.deleteThietBi(data.id,function(){
        res.sendStatus(200);
    })
})
router.post('/lichsu',function(req,res)
{
    var data={};
    data.id=req.body.id;
    thietbiController.getDVSD(data.id,function(taisan){
        var lichsu={};
        lichsu.DonViSuDung=taisan.TaiSan.DonViSuDung;
        lichsu.TS_MaThietBi=data.id;
        lichsu.loai="Giam"
        lichsuController.createLichsu(lichsu,function(err){
            if(err) throw err.status;
        });
    })
    res.sendStatus(200);
});
router.post('/thanhly',function(req,res){
    var data={};
    data.id=req.body.id;
    thietbiController.ThanhLyThietbi(data,function(){
        res.sendStatus(200);
    })
})
router.post('/',function(req,res){
    var data={};
    var array=[];
    data.TenThietBi=req.body.TenThietBi;
    data.GiaTri=req.body.GiaTri;
    data.MaDVSD=req.body.MaDVSD;
    data.NamSuDung=req.body.NamSuDung;
    data.TinhTrang=req.body.TinhTrang;
    data.TS_MaTaiSan=req.body.TS_MaTaiSan;
    for(i=0;i<req.body.soluong;i++){
        array.push(data);
    }
    thietbiController.createMultiThietBi(array,function(err){
        if(err) res.sendStatus(500);
        else res.sendStatus(200); 
    })
})
router.patch('/tlyts',function(req,res){
    var madon=parseInt(req.body.madon);
    denghiController.getByID(madon,function(data){
        var async=require('async');
        var array=data.dsIdThietBiLienQUan;
        array=array.split(',');
        var uniq=[];
        async.forEachOf(array,function(value,key,callback){
            var data={};
            data.id=parseInt(value);
            thietbiController.ThanhLyThietbi(data,function(){});
            thietbiController.getById(parseInt(value),function(thietbi){
                var tmpTb={};
                tmpTb.TS=thietbi.TS_MaTaiSan;
                tmpTb.GiaTri=thietbi.GiaTri;
                pushToArray(uniq,tmpTb);
                callback(null);
                if(key+1==array.length){
                    uniq.forEach(element=>{
                        var data={};
                        data.Loai=0;
                        data.TS_MaTaiSan=element.TS;
                        data.GiaTri=element.GiaTri;
                        data.MaDeNghi=madon;
                        lichsuController.createLichsu(data,function(){
                        })
                    })
                    res.sendStatus(200);
                }
            })
        })
        
    })
})

function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}
function pushToArray(arr,obj){
    const index=arr.findIndex(item=>item.TS===obj.TS);
    if(index>-1) {arr[index].GiaTri=arr[index].GiaTri+obj.GiaTri}
    else {arr.push(obj)};
}
async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }
module.exports=router;