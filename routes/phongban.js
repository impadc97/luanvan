var express = require('express');
var router = express.Router();
var phongbanController = require('../controllers/phongbanController');


router.get('/raw', function (req, res){
    phongbanController.getAll(function(phongbans){
        res.send(phongbans);
    })
})

module.exports = router;