var express = require('express');
var router = express.Router();
var taisanController=require('../controllers/taisanController');
var lichsuController=require('../controllers/lichsuController');
var denghiController=require('../controllers/denghiController');
var thietbiController=require('../controllers/thietbiController');
var json2xls=require('json2xls');
var fs=require('fs');
router.get('/',function(req,res)
{
    taisanController.getAll(function(taisan){
        denghiController.getPhongBan(function(phongban){
            res.render('index',{
                title:'Tai San',
                taisan:taisan,
                phongban:phongban,
            })
        });
    })
})
router.get('/pb/:id/',function(req,res){
    taisanController.TSbyPhong(req.params.id,function(taisan){
        denghiController.getPhongBan(function(phongban){
            res.render('tsPhongban',{
                title:'TS Phong ban',
                taisan:taisan,
                phongban:phongban,
            })
        });
    })
})
router.get('/json',function(req,res){
    taisanController.getAll(function(taisan){
        res.json(taisan);
    })
})
router.get('/raw',function(req,res){
        taisanController.getAll(function(taisan){
            res.send(taisan)
        })
    })
router.get('/export',function(req,res)
{
    taisanController.getAll(function(taisan){
        var obj=taisan;
        var xls=json2xls(obj);
        fs.writeFileSync('data.xlsx',xls,'binary');
        res.download('./data.xlsx', 'data.xlsx');
    })
})
router.post('/',function(req,res){
    var tmp={};
    tmp.MaDeNghi=req.body.MaDeNghi;
    tmp.TenTaiSan=req.body.TenTaiSan;
    tmp.NamSanXuat=req.body.NamSanXuat;
    tmp.MaPhong=req.body.MaPhong;
    tmp.ThoiHanBaoHanh=req.body.ThoiHanBaoHanh;
    taisanController.createTaisan(tmp,function(data){
            res.send(data);
    });
})
router.delete('/',function(req,res){
    var data={};
    data.id=req.body.id;
    taisanController.deleteTaiSan(data.id,function(err){
        if(err) res.sendStatus(500);
        else {
            res.sendStatus(200);
            res.end();
        }
    })
})
router.put('/',function(req,res)
{
    var editContent={};
            editContent.id=req.body.id;
            editContent.TenTaiSan=req.body.TenTaiSan;
            editContent.NamSanXuat=req.body.NamSanXuat;
            editContent.Model=req.body.Model;
            editContent.MaDeNghi=req.body.MaDeNghi;
    taisanController.updateTaisan(editContent,function(){
        res.sendStatus(200);
        res.end();
    })
})
router.get('/:id',function(req,res){
    taisanController.getInfo(req.params.id,function(taisan){
        denghiController.getBoMon(taisan.MaPhong,function(bomon){
            denghiController.getAllBoMon(function(fullbomon){
                res.render('detailTaiSan',{
                    title:'Chinh sua tai san',
                    taisan:taisan,
                    bomon:bomon,
                    fullbomon:fullbomon,
                }) 
            })
        })
    })
})
router.put('/:id',function(req,res){
    var dtajax={};
    dtajax.array=req.body.array;
    dtajax.MaTaiSan=req.body.MaTaiSan;
    dtajax.bomon=req.body.bomon;
    dtajax.MaPhong=req.body.MaPhong;
    dtajax.dvsd=req.body.bomon; // dvsd moi
    denghiController.getBoMonbyID(dtajax.dvsd,function(dvsdnew){
        if(dvsdnew.IDPhongBan==dtajax.MaPhong) {
            for(var i=0;i<dtajax.array.length;i++){
                thietbiController.chuyenThietBi(dtajax.array[i],dtajax.dvsd,dtajax.MaTaiSan,function(err){
                    if(err) res.sendStatus(500);
                        else 
                        {
                            res.sendStatus(200);
                            res.end();
                        }
                }); // neu chuyen trong cung 1 phong ban
            }
        }
        else { // chuyen khac phong ban
            taisanController.getById(dtajax.MaTaiSan,function(data){
                var newTaiSan={};
                newTaiSan.Model=data.Model;
                newTaiSan.TenTaiSan=data.TenTaiSan;
                newTaiSan.NamSanXuat=data.NamSanXuat;
                newTaiSan.XuatXu=data.XuatXu;
                newTaiSan.MaDeNghi=data.MaDeNghi;
                newTaiSan.ThoiHanBaoHanh=data.ThoiHanBaoHanh;
                newTaiSan.MaPhong=dvsdnew.IDPhongBan;
                taisanController.createTaisan(newTaiSan,function(newts){ // tao tai san moi
                    for(var i=0;i<dtajax.array.length;i++)
                    thietbiController.chuyenThietBi(dtajax.array[i],dtajax.dvsd,newts.id,function(err){
                        var restb={};
                        restb.id=newts.id;
                            res.send(restb);
                    });
                })
            })
        }
    })
})
router.post('/lichsu',function(req,res){
    var data={};
    data.id=req.body.id;
    var tmp={};
    tmp.DonViSuDung="temp";
    tmp.TS_MaThietBi=data.id;
    tmp.loai="Giam";
    lichsuController.createLichsu(tmp,function(err){
        if(err) throw err.status;
    });
    res.sendStatus(200);
})

module.exports=router;