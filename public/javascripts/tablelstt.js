$(document).ready(function(){
    var table2=$('#tablelstt').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
            ],
        buttons: [
            'excelHtml5', 'print'
        ],
        columnDefs:[{targets:4, render:function(data){
            return moment(data).format('MMMM Do YYYY');
          }}],
    });
    $('#tablelstt').show();
})