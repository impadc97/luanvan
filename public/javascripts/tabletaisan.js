$(document).ready(function() {
    var currow;
    var slid;
    $('#PhongBanTS').on('change',function(){
        var idpb=parseInt($('#PhongBanTS').val());
        window.location.href="/taisan/pb/"+idpb;
    })
    $('#btn_XoaTaiSan').on('click',function(){
        var answer = window.confirm("Bạn có muốn xoá tài sản và những thiết bị thuộc tài sản này?")
        if (answer) {
            var data={};
            data.id=slid;
            $.ajax({
                url:'/taisan',
                type:'DELETE',
                data:JSON.stringify(data),
                contentType:'application/json',
                success:function(){
                    $.notify("Xoá thành công","success");
                    tabletaisan.row(currow).remove().draw();
                },
                error:function(){
                    $.notify("Xoá thất bại","error");
                }
            })
        }
        else {
        }
    })
    $('#btn_ChuyenThietBi').on('click',function(){
        var array=[];
        var tonggiatri=0;
        var id=$('#idthietbi').text();
        $('input[type=checkbox]').each(function(){
            if(this.checked){
                array.push($(this).val());
                tonggiatri=tonggiatri+parseInt($(this).attr("data-giatri"));
            } 
            console.log(tonggiatri);
        });
        var data={};
        var bomon=parseInt($('#DVSDChuyen').val());
        data.MaTaiSan=$('#idthietbi').text();
        data.array=array;
        data.bomon=bomon;
        data.MaPhong=parseInt($('#phongban').text());
        if(array.length!=0){
            var url='/taisan/'+id;
            $.ajax({
                url:url,
                type:'PUT',
                data:JSON.stringify(data),
                contentType:'application/json',
                success:function(res){
                    var content={};
                    content.Loai=2;
                    content.MaTaiSanCu=data.MaTaiSan;
                    content.TS_MaTaiSan=res.id;
                    content.GiaTri=tonggiatri;
                    $.ajax({
                        url:'/lichsu',
                        type:'POST',
                        data:JSON.stringify(content),
                        contentType:'application/json',
                        success:function(){
                            $.notify("Chuyển thành công","success");
                        }
                    })
                },
                error:function(){
                    $.notify("Chuyển thất bại","error");
                }
            })
        }
    })
    $('#btn_themTaiSan').on('click',function(){
        var data={};
        data.id=$('#id').val();
        data.MaHoSo=$('#MaHoSo').val();
        data.TenTaiSan=$('#TenTaiSan').val();
        data.Model=$('#Model').val();
        data.NamSanXuat=$('#NamSanXuat').val();
        if(data.NamSanXuat=="") data.NamSanXuat=null;
        data.PhongBan=parseInt($('#PhongBan').val());
        console.log(data);
        $.ajax({
            url:'/taisan',
            type:'POST',
            data:JSON.stringify(data),
            contentType:'application/json',
            success:function(){
                $.notify("Thêm thành công","success");
            },
            error:function(){
                $.notify("Thêm thất bại","error");
            }
        });
    })
    $('#tabletaisan thead tr').clone(true).appendTo( '#tabletaisan thead' );
    $('#tabletaisan thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( tabletaisan.column(i).search() !== this.value ) {
                tabletaisan
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
    var tabletaisan=$('#tabletaisan').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        dom: 'Blfrtip',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
            ],
        buttons: [
            'excelHtml5', 'print'
        ],
        processing : true,
    });
    $('#tabletaisan').show();
    $('#tabletaisan tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            tabletaisan.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            tmp=tabletaisan.row(this).data();
            currow=tabletaisan.row(this).index();
            slid=tmp[0].substring(tmp[0].indexOf('>')+1,tmp[0].lastIndexOf('<'));
        }
    } );
    function updateTS(updatedCell,updatedRow,oldValue)
    {
        if(updatedCell.data()!=oldValue)
        {
            var result=confirm("Xác nhận sửa dữ liệu!");
            if(result==true){
                var newvalue=updatedRow.data();
                var data={};
                for(i=0;i<newvalue.length;i++){
                    if(newvalue[i]=='') newvalue[i]=null;
                }
                data.id=newvalue[0].substring(newvalue[0].indexOf('>')+1,newvalue[0].lastIndexOf('<'));
                data.TenTaiSan=newvalue[1];
                data.Model=newvalue[2];
                data.NamSanXuat=newvalue[3];
                data.MaDeNghi=parseInt(newvalue[7]);
                $.ajax({
                    url:'/taisan',
                    type:'PUT',
                    data:JSON.stringify(data),
                    contentType: 'application/json',
                    error:function(){
                        updatedCell.data(oldValue);
                    }
                })
            }
            else{
                updatedCell.data(oldValue);
            }
        }
    }
    tabletaisan.MakeCellsEditable({
        "onUpdate":updateTS,
        "columns": [1,2,3,7],
        "inputTypes": [
            {
                "column": 1,
                "type": "text",
                "options": null
            },
            {
                "column":2, 
                "type": "text",
                "options":null
            },
            {
                "column": 3,
                "type": "datepicker", // requires jQuery UI: http://http://jqueryui.com/download/
                "options": {
                    "icon": "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif" // Optional
                }
            },
            {
                "column":7, 
                "type": "number",
                "options":null
            },
            {
                "column":8,
                "type":"text",
                "options":null
            },
            {
                "column": 9,
                "type": "datepicker", // requires jQuery UI: http://http://jqueryui.com/download/
                "options": {
                    "icon": "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif" // Optional
                }
            },
            {
                "column":10, 
                "type": "number",
                "options":null
            },
        ],
    })
    $('#btn-deleteTS').on('click',function(){
        tabletaisan.row(currow).remove().draw();
        var content={};
        content.id=slid;
        $.ajax({
            url:'/taisan',
            type:'DELETE',
            data: JSON.stringify(content),
            contentType: 'application/json',
            sucess:function(){
                tabletaisan.row(currow).remove().draw();
            }
        })
    })
});