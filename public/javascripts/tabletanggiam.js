
$(document).ready(function(){
    var table=$('#tabletanggiam').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
            ],
            buttons: [
                { extend: 'excelHtml5', className: 'btn btn-info btn-sm',exportOptions: {
                    columns: [ 0, 1,2,3,4,5,6 ]} },
                { extend: 'print', className: 'btn btn-info btn-sm',exportOptions: {
                    columns: [ 0, 1,2,3,4,5,6]}}
            ],
        columnDefs:[{targets:6, render:function(data){
            return moment(data).format('MM DD YYYY');
          }},{targets:[3,4],searchable:false}],
          "footerCallback": function ( settings) {
            var api = this.api();
 
            // var tongtang=0;
            // var tonggiam=0;
            // test=api.column(2).every(function(){
            //     if(api.column(1).search('Tăng')) tongtang=tongtang+parseInt(this.data);
            //     return tongtang;
            // })

            // Total over all pages
            var totaltang = api
            .cells( function ( index, data, node ) {
                return api.row( index ).data()[7] === '1' ?
                    true : false;
            }, 2, { search: 'applied' } )
            .data()
            .sum();

            var totalgiam = api
            .cells( function ( index, data, node ) {
                return api.row( index ).data()[7] === '0' || api.row( index ).data()[7] === '2'?
                    true : false;
            }, 2, { search: 'applied' } )
            .data()
            .sum();
 
            // Update footer
            $('#tstang').html(
                'Tài sản tăng: '+numberWithCommas(totaltang)+' VND'
            );
            $('#tsgiam').html(
                'Tài sản giảm: '+numberWithCommas(totalgiam)+' VND'
            );
        }
    });
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var min = $('#min').datepicker("getDate");
            var max = $('#max').datepicker("getDate");
            var startDate = new Date(data[6]);
            if (min == null && max == null) { return true; }
            if (min == null && startDate <= max) { return true;}
            if(max == null && startDate >= min) {return true;}
            if (startDate <= max && startDate >= min) { return true; }
            return false;
        }
    );
    $("#min").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
    $("#max").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });

    // Event listener to the two range filtering inputs to redraw on input
    $('#min, #max').change(function () {
        table.draw();
    });
    // $('#tabletanggiam thead tr').clone(true).appendTo( '#tabletanggiam thead' );
    // $('#tabletanggiam thead tr:eq(1) th').each( function (i) {
    //     var title = $(this).text();
    //     $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
 
    //     $( 'input', this ).on( 'keyup change', function () {
    //         if ( table.column(i).search() !== this.value ) {
    //             table
    //                 .column(i)
    //                 .search( this.value )
    //                 .draw();
    //         }
    //     } );
    // } );
    $('#tabletanggiam').show();
    // var sum=table.api().cells(function(index,data,node){
    //     return table.api().row(index).data()[1]==='Tăng tài sản'?true:false;},0,{search:'applied'}).data().sum();
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
})