$(document).ready(function () {
    var tablethietbi = $('#tablethietbisohuu').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'Show all']
        ],
        buttons: [
            { extend: 'excelHtml5', className: 'btn btn-info btn-sm' },
            { extend: 'print', className: 'btn btn-info btn-sm'}
        ],
    });
    $('#tablethietbisohuu').show();
});
