$(document).ready(function () {
    var tablethietbi = $('#tablethietbi').DataTable({
        paging: false,
    });
    var datatmp = {};
    datatmp.dstb = new Array;
    $(".cbtb").click(function () {
        datatmp.dstb = [];
        $('.cbtb:checked').each(function () {
            datatmp.dstb.push($(this).val());
        });
    });

    var tablethietBiTimduoc = $('#tablethietbitimduoc').DataTable({
        paging: false,
    });


    $('#btn_exportWord').on('click', function () {
        datatmp.loaidon = $('#Loai').val();
        datatmp.noidung = $('#NoiDung').val();
        datatmp.nguoilienhe = $('#NguoiLienHe').val();
        datatmp.sdt = $('#SDT').val();
        datatmp.nguonkinhphi = $('#NguonKinhPhi').val();
        datatmp.ngaydenghi = $('#NgayDeNghi').val();
        datatmp.donvi=$('#ipDonVi').val();
        $.ajax({
            url: '/denghisohuu/export',
            type: 'post',
            data: JSON.stringify(datatmp),
            contentType: 'application/json',
            success: function () {
                window.location.href = "export";
            },
            error: function () {
                $.notify("Thêm thất bại", "error");
            }
        })
    })
    // $('#Loai').on('change', function (e) {

    //     var optionSelected = $("option:selected", this);
    //     var loaiselected = this.value;
    //     console.log(loaiselected);
    //     if (loaiselected == 3) {
    //         $('#btn_TSTL').show();
    //     }
    //     else { $('#btn_TSTL').hide(); }
    // })

    //
    $('#timthietbi-area').hide();
    $('#Loai').on('change', function () {
        if (this.value == "2" || this.value=="3") {
            $('#timthietbi-area').show();
        }
        else {
            $('#timthietbi-area').hide();
        }
    });

    //Lay danh sach thiet bi
    var thietbis = new Array();

    var bomons = new Array();
    $('#btn_timthietbi').on('click', function () {
        // $.get("/thietbisohuu/json", function (data, status) {
        //     if (status == "success") {

        //         thietbis = data;
        //         let options = '';
        //         for (var i = 0; i < thietbis.length; i++) {
        //             options += '<option value="' + thietbis[i].id + '" />';
        //             // tablethietbi.row.add([
        //             //     thietbis[i].id,
        //             //     thietbis[i].TenThietBi,
        //             //     thietbis[i].NamSuDung,
        //             //     thietbis[i].GiaTri,
        //             //     thietbis[i].TinhTrang

        //             // ]).draw(false);
        //         }
        //         document.getElementById('thietbis').innerHTML = options;
        //     }
        // })

        $.get("/bomon/phongban/curuser/", function (data, status) {
            if (status == "success") {
                bomons = data;
                let options = '';
                for (var i = 0; i < bomons.length; i++) {
                    options += `<option value=${bomons[i].id}>${bomons[i].TenBoMon}</option>`;
                }
                document.getElementById('bomons').innerHTML = options;
            }
        })
    })
    $('#bomon-search').on('input', function () {

        let selectedId = this.value;
        for (let i = 0; i < bomons.length; i++) {

            if (bomons[i].id == selectedId) {
                let selectedItem = thietbis[i];
                // $('#dialog-thietbi-tenthietbi').html(`<b>Tên thiết bị:</b> ${selectedItem.TenThietBi}`);
                // $('#dialog-thietbi-namsudung').html(`<b>Năm sử dụng:</b> ${selectedItem.NamSuDung}`);
                // $('#dialog-thietbi-giatri').html(`<b>Giá trị:</b> ${selectedItem.GiaTri}`);

                tablethietbi.column(1).search(bomons[i].id).draw();
                break;
            }
        }

    })

    $('#btn_themThietBi').on('click', function () {
        //$("#Thietbi").val(`${datatmp.dstb}`);
        // tablethietbi.row.add([
        //     thietbis[i].id,
        //     thietbis[i].TenThietBi,
        //     thietbis[i].NamSuDung,
        //     thietbis[i].GiaTri,
        //     thietbis[i].TinhTrang

        // ]).draw(false);

        tablethietBiTimduoc.clear();

        for (let i = 0; i < datatmp.dstb.length; ++i) {
            tablethietBiTimduoc.row.add([
                datatmp.dstb[i]
            ]).draw(false);
        }

        $('#NoiDung').val(`Danh sách mã thiết bị: ${datatmp.dstb} `);
        $('#NoiDung').parent().addClass('is-filled');

    })

    var fileAnhDonDenghiCanXetDuyet;

    $('#btn_luuAnhDonDenghi').on('click', function () {
        var file = document.getElementById("imgInp").files[0];
        var formData = new FormData();
        formData.append("file", file);
        console.log(formData);

        $.ajax({
            url: '/denghisohuu/add/uploads',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (rs) {
                fileAnhDonDenghiCanXetDuyet = rs;
                md.showNotification(`Up ảnh thành công`, 'bottom', 'right', 'rose');
            },
            error: function () {
                console.log('error');
            }
        })
    });

    
    $('#btn_themDeNghi,#btn_Save').on('click', function () {
        var data = {};

        var loaidenghi = document.getElementById('Loai');
        var sdkp = document.getElementById('NguonKinhPhi');

        var dataDondenghi = {};
        dataDondenghi.noidunghoso = $('#NoiDung').val();
        dataDondenghi.ngaydenghi = $('#NgayDeNghi').val();
        dataDondenghi.donDdnghixetduyetimgpath = fileAnhDonDenghiCanXetDuyet;
        //data.ngaynhandenghi=$('#NgayNhanDeNghi').val();
        //data.namthuchien=$('#NamThucHien').val();
        dataDondenghi.loaidenghi = parseInt(loaidenghi.value);
        dataDondenghi.sdkp = sdkp.value;
        dataDondenghi.tinhtrang = 1;
        dataDondenghi.dsidthietbicansua = datatmp.dstb.toString();
        data.datadondenghi = dataDondenghi;


        // if(data.ngaydenghi=="") data.ngaydenghi=null;
        // if(data.ngaynhandenghi=="") data.ngaynhandenghi=null;
        // if(data.namthuchien=="") data.namthuchien=null;
        // if(data.namthuchien!==data.namthuchien) data.namthuchien=null;
        //alert(data);
        //console.log(data);
        $.ajax({
            url: '/denghisohuu/add',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: function () {
                md.showNotification("Thêm thành công", 'bottom', 'right', 'rose');
            },
            error: function () {
                md.showNotification("Thêm thất bại", 'bottom', 'right', 'rose');
            }
        });
    });

    $('#btn-taobannhap').on('click', function () {
        var data = {};

        var loaidenghi = document.getElementById('Loai');
        var sdkp = document.getElementById('NguonKinhPhi');

        if (loaidenghi.value == "2") {
            var dataDondenghi = {};
            dataDondenghi.noidunghoso = $('#NoiDung').val();
            dataDondenghi.ngaydenghi = $('#NgayDeNghi').val();
            //data.ngaynhandenghi=$('#NgayNhanDeNghi').val();
            //data.namthuchien=$('#NamThucHien').val();
            dataDondenghi.loaidenghi = parseInt(loaidenghi.value);
            dataDondenghi.sdkp = sdkp.value;
            dataDondenghi.tinhtrang = -1;

            data.datadondenghi = dataDondenghi;
            data.idthietbicansua = $('#Thietbi').val();

            // if(data.ngaydenghi=="") data.ngaydenghi=null;
            // if(data.ngaynhandenghi=="") data.ngaynhandenghi=null;
            // if(data.namthuchien=="") data.namthuchien=null;
            // if(data.namthuchien!==data.namthuchien) data.namthuchien=null;
            //alert(data);
            //console.log(data);
            $.ajax({
                url: '/denghisohuu/addsuachua',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function () {
                    md.showNotification("Thêm bản nháp thành công", 'bottom', 'right', 'rose');
                },
                error: function () {
                    md.showNotification("Thêm bản nháp thất bại", 'bottom', 'right', 'rose');
                }
            });
        }
        else {
            data.noidunghoso = $('#NoiDung').val();
            data.ngaydenghi = $('#NgayDeNghi').val();
            //data.ngaynhandenghi=$('#NgayNhanDeNghi').val();
            //data.namthuchien=$('#NamThucHien').val();
            data.loaidenghi = parseInt(loaidenghi.value);
            data.sdkp = sdkp.value;
            data.tinhtrang = -1;

            // if(data.ngaydenghi=="") data.ngaydenghi=null;
            // if(data.ngaynhandenghi=="") data.ngaynhandenghi=null;
            // if(data.namthuchien=="") data.namthuchien=null;
            // if(data.namthuchien!==data.namthuchien) data.namthuchien=null;
            //alert(data);
            //console.log(data);
            $.ajax({
                url: '/denghisohuu/add',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function () {
                    md.showNotification("Thêm bản nháp thành công", 'bottom', 'right', 'rose');
                },
                error: function () {
                    md.showNotification("Thêm bản nháp thất bại", 'bottom', 'right', 'rose');
                }
            });
        }
    });

    $('#btn-luu').on('click', function () {
        var data = {};

        var loaidenghi = document.getElementById('Loai');
        var sdkp = document.getElementById('NguonKinhPhi');

        if (loaidenghi.value == "2") {
            var dataDondenghi = {};
            dataDondenghi.noidunghoso = $('#NoiDung').val();
            dataDondenghi.ngaydenghi = $('#NgayDeNghi').val();
            //data.ngaynhandenghi=$('#NgayNhanDeNghi').val();
            //data.namthuchien=$('#NamThucHien').val();
            dataDondenghi.loaidenghi = parseInt(loaidenghi.value);
            dataDondenghi.sdkp = sdkp.value;
            dataDondenghi.tinhtrang = -1;

            data.datadondenghi = dataDondenghi;
            data.idthietbicansua = $('#Thietbi').val();

            // if(data.ngaydenghi=="") data.ngaydenghi=null;
            // if(data.ngaynhandenghi=="") data.ngaynhandenghi=null;
            // if(data.namthuchien=="") data.namthuchien=null;
            // if(data.namthuchien!==data.namthuchien) data.namthuchien=null;
            //alert(data);
            //console.log(data);
            $.ajax({
                url: '/denghisohuu/addsuachua',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function () {
                    // md.showNotification("Thêm bản nháp thành công", 'bottom', 'right', 'rose');
                    document.location.href = "/denghisohuu";
                },
                error: function () {
                    md.showNotification("Thêm bản nháp thất bại", 'bottom', 'right', 'rose');
                }
            });
        }
        else {
            data.noidunghoso = $('#NoiDung').val();
            data.ngaydenghi = $('#NgayDeNghi').val();
            //data.ngaynhandenghi=$('#NgayNhanDeNghi').val();
            //data.namthuchien=$('#NamThucHien').val();
            data.loaidenghi = parseInt(loaidenghi.value);
            data.sdkp = sdkp.value;
            data.tinhtrang = -1;

            // if(data.ngaydenghi=="") data.ngaydenghi=null;
            // if(data.ngaynhandenghi=="") data.ngaynhandenghi=null;
            // if(data.namthuchien=="") data.namthuchien=null;
            // if(data.namthuchien!==data.namthuchien) data.namthuchien=null;
            //alert(data);
            //console.log(data);
            $.ajax({
                url: '/denghisohuu/add',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function () {
                    md.showNotification("Thêm bản nháp thành công", 'bottom', 'right', 'rose');
                },
                error: function () {
                    md.showNotification("Thêm bản nháp thất bại", 'bottom', 'right', 'rose');
                }
            });
        }
    });

    /**
     *  SOCKETIO HANDLE
     */

    var curUser = document.getElementById('current-user');
    //console.log(curUser.dataset.userid);

    var socket = io.connect("http://localhost:3000");

    //var room = `${curUser.dataset.userid}`;
    //socket.emit('test', "ok");

    //socket.on('message', function (data) {
    //    md.showNotification(data, 'bottom', 'right', 'rose');
    //});

    socket.on('message-receive-donvisudung', function (data) {
        md.showNotification(data, 'bottom', 'right', 'rose');
    });

    $('#Loai').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        if (valueSelected == 3) {
            $('#btn_TSTL').show();
        }
        else { $('#btn_TSTL').hide(); }
    })



})