$(document).ready(function(){
    $.ajax({
        url:'/navbar',
        type:'GET',
        success:function(res){
            $('#navbarnoti').html(res);
        }
    })
    $('#btn_luutrangthai').on('click',function(){
        var data={};
        var pathname=window.location.pathname;
        data.id=pathname.substr(pathname.lastIndexOf('/')+1);
        data.TinhTrang=$('#input_tinhtrang').val();
        console.log(JSON.stringify(data));
        $.ajax({
            url:'/thietbi/'+data.id,
            type:'PUT',
            data:JSON.stringify(data),
            contentType:'application/json',
            success:function(){
                $.ajax({
                    url:'/lskk',
                    type:'POST',
                    data:JSON.stringify(data),
                    contentType:'application/json',
                    success:function(){
                        location.reload();
                    }
                })
            }
        })
    })
    $('#btnRegisUser').on('click',function(){
        var username=$('#ip_usernamereg').val();
        var password=$('#ip_pwdreg').val();
        var cfpwd=$('#ip_cfpwd').val();
        var hoten=$('#ip_hoten').val();
        var sdt=$('#ip_sdt').val();
        var email=$('#ip_email').val();
        
        if(username == '' || password == ''||cfpwd==''||hoten =='' ||sdt =='' ||email =='') alert('Nhap thieu thong tin');
        else if(password != cfpwd) alert('Mat khau nhap lai khong chinh xac')
        else if (password.length < 4) alert('Mat khau qua ngan')
        else 
        {
            var data={};
            data.username=username;
            data.password=password;
            data.hoten=hoten;
            data.phone=sdt;
            data.email=email;
            $.ajax({
                url:'/users/register',
                type:'POST',
                data:JSON.stringify(data),
                contentType:'application/json',
            })
        }
    })
    $('#btn_kiemke').on('click',function(){
        var pathname=window.location.pathname;
        var data={};
        data.id=pathname.substr(pathname.lastIndexOf('/')+1);
        data.TinhTrang='Đã kiểm ' +new Date().getFullYear();
        $.ajax({
            url:'/thietbi/'+data.id,
            type:'PUT',
            data:JSON.stringify(data),
            contentType:'application/json',
            success:function(){
                location.reload();
            }
        })
    })
})

    