$(document).ready(function () {


    var InfoDon = document.getElementById('InfoDon');
    var idnguoixuly = InfoDon.dataset.idnguoixuly;
    var statusTable = document.getElementById('tablestatus');
    var curstatus = parseInt(statusTable.dataset.tinhtrang);

    //Kiem tra don da co ai xu ly chua
    //Neu chua co nguoi xu ly don
    if (!idnguoixuly) {

        $('.toggle-huydon').hide();
        $('#info-nguoixulydon').hide();
        $('#status-tracking-detail').hide();

    }
    else {
        $('#dt-duyetdon').hide();
        $('#btn-reviewdon').hide();

    }
    //Neu don da bi huy
    if (curstatus == 0) {
        $('#dt-duyetdon').hide();
        $('#btn-reviewdon').hide();
        $('.toggle-huydon').hide();
        $('#status-tracking-detail').hide();
        $('#status-tracking').hide();
        $('#MaDon').append(`<b> (Đã hủy) </b>`);
    }


    //Xu ly table status theo trang thai don de nghi

    var fileAnhDonDenfinal;

    $('#btn_luuAnhDonDenghiFinal').on('click', function () {

        var file = document.getElementById("imgInp").files[0];
        var formData = new FormData();
        formData.append("file", file);
        console.log(formData);

        $.ajax({
            url: '/denghisohuu/add/uploadsfinal',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (rs) {
                fileAnhDonDenfinal = rs;
                md.showNotification(`Up ảnh thành công`, 'bottom', 'right', 'rose');
            },
            error: function () {
                console.log('error');
            }
        })
    });

    var fileDsBaogia;

    $('#btn_luudsbaogia').on('click', function () {

        let baogiafiles = document.getElementById("baogias").files;
        let baogiaData = new FormData();
        for (let i = 0; i < baogiafiles.length; ++i) {
            baogiaData.append("baogias", baogiafiles[i]);
        }

        console.log(baogiaData);

        $.ajax({
            url: '/denghi/uploadsdsbaogia',
            type: 'POST',
            data: baogiaData,
            contentType: false,
            processData: false,
            success: function (rs) {
                fileDsBaogia = rs;
            },
            error: function () {
                console.log('error');
            }
        })
    });

    $('#btn-luudsbaogia_ok').on('click', function () {
        let content = {}
        //id don de nghi
        let id = parseInt(document.getElementById("MaDon").dataset.madon);

        content.id = id;

        let dsBaogiaString = "";
        for (let i = 0; i < fileDsBaogia.length; ++i) {
            if (i !== fileDsBaogia.length - 1) {
                dsBaogiaString += `${fileDsBaogia[i].filename},`;
            }
            else {
                dsBaogiaString += `${fileDsBaogia[i].filename}`;
            }
        }
        content.dsbaogiapath = dsBaogiaString;

        $.ajax({
            url: '/denghi/luudsbaogia',
            type: 'PATCH',
            data: JSON.stringify(content),
            contentType: 'application/json',
            success: function () {

                //Thong bao cho donvisudung

                // var socket = io.connect("http://localhost:3000");

                // socket.emit('message-to-donvisudung', "Nhân viên đã tiếp nhận đơn của bạn");

                // md.showNotification("Duyệt đơn thành công","left","center","rose");
                window.location.reload(true);
            }
        })
    })

    for (let i = 1; i < statusTable.rows.length; ++i) {
        if (i <= curstatus) {
            statusTable.rows[i].cells[2].innerHTML = `Hoàn tất`;

        }
        statusTable.rows[i].cells[2].style.cssText = "text-align:center;width:150px;height: auto;";
    }

    $('.dt-completestep').each(function () {
        $(this).on('click', function (evt) {
            let clickedStep = parseInt(this.dataset.step);

            if (clickedStep == curstatus + 1) {
                let nextStep = confirm("Bạn muốn hoàn tất bước này?");
                if (nextStep) {
                    let content = {}
                    //id don de nghi
                    let id = parseInt(document.getElementById("MaDon").dataset.madon);

                    content.id = id;
                    content.curstatus = curstatus;

                    switch (curstatus) {
                        case 2: {
                            content.fileanhfinal = fileAnhDonDenfinal;
                        } break;
                    }

                    console.log(content);
                    $.ajax({
                        url: '/denghi/nextstep',
                        type: 'PATCH',
                        data: JSON.stringify(content),
                        contentType: 'application/json',
                        success: function () {

                            //Thong bao cho donvisudung

                            // var socket = io.connect("http://localhost:3000");

                            // socket.emit('message-to-donvisudung', "Nhân viên đã tiếp nhận đơn của bạn");

                            // md.showNotification("Duyệt đơn thành công","left","center","rose");
                            window.location.reload(true);
                        }
                    })
                }
            }
            else {
                alert("Bạn phải thực hiện các bước tuần tự !");
            }

        });
    });



    //In don de nghi buttons
    $('.dt-inmau').each(function () {
        $(this).on('click', e => {
            var url = "/print/mauso/" + this.dataset.mauso;
            window.open(url, "_blank");
        })
    });

    //Xem don de nghi buttons
    $('#btn-reviewdon').on('click', function () {
        let imgpath = this.dataset.imgpath;
        let url = 'http://localhost:3000/assests/img/dondenghi/banchoxetduyet/' + imgpath;
        $('#DocImg').attr("src", url);
    });

    //Xem don de nghi buttons
    $('#btn-xemdondenghixetduyet').on('click', function () {
        let imgpath = this.dataset.imgpath;
        let url = 'http://localhost:3000/assests/img/dondenghi/banchoxetduyet/' + imgpath;
        $('#DocImg').attr("src", url);
    });

    //Xem don de nghi final buttons
    $('#btn-xemdondenghifinal').on('click', function () {
        let imgpath = this.dataset.imgpath;
        let url = 'http://localhost:3000/assests/img/dondenghi/final/' + imgpath;
        $('#DocImg').attr("src", url);
    });


    $('#btn_themThietBi').on('click', function () {
        var taisan = {};
        var currtime=new Date().getFullYear();
        taisan.MaPhong=parseInt(document.getElementById("MaDon").dataset.maphong);
        taisan.MaDeNghi=parseInt(document.getElementById("MaDon").dataset.madon);
        taisan.TenTaiSan = ($('#ipTenTS').val());
        taisan.NamSanXuat = ($('#ipNSX').val());
        taisan.ThoiHanBaoHanh = ($('#ipBH').val());
        var soluong = parseInt($('#ipamount').val());
        var thietbi = {};
        thietbi.TenThietBi = $('#ipTenTB').val();
        thietbi.GiaTri = parseInt($('#ipGiaTriTB').val());
        thietbi.MaDVSD = $('#DVSD').val();
        thietbi.NamSuDung=currtime;
        thietbi.TinhTrang="Bình thường";
        thietbi.soluong=soluong;
        $.ajax({
            url: '/taisan',
            type: 'POST',
            data: JSON.stringify(taisan),
            contentType: 'application/json',
            success: function (data) {
                thietbi.TS_MaTaiSan=data.id;
                $.ajax({
                    url:'/thietbi',
                    type:'POST',
                    data:JSON.stringify(thietbi),
                    contentType:'application/json',
                    success:function(){
                        var datals={};
                        datals.TS_MaTaiSan=thietbi.TS_MaTaiSan;
                        datals.Loai=1;
                        datals.GiaTri=thietbi.GiaTri;
                        datals.MaDeNghi=taisan.MaDeNghi;
                        datals.MaDeNghi=parseInt(document.getElementById("MaDon").dataset.madon);
                        $.ajax({
                            url:'/lichsu',
                            type:'POST',
                            data:JSON.stringify(datals),
                            contentType:'application/json',
                            success:function(){
                                $.notify("Thêm thành công","success");
                            }
                        })
                    }
                })
            },
            error: function () {
                $.notify("Thêm thất bại", "error");
            }
        })
    })
});

//Duyet don denghi row buttons
$('#dt-duyetdon').click(function () {
    var content = {};
    //id don de nghi
    let id = parseInt(document.getElementById("MaDon").dataset.madon);

    content.id = id;
    $.ajax({
        url: '/denghi/duyetdon',
        type: 'PATCH',
        data: JSON.stringify(content),
        contentType: 'application/json',
        success: function () {

            //Thong bao cho donvisudung

            // var socket = io.connect("http://localhost:3000");

            // socket.emit('message-to-donvisudung', "Nhân viên đã tiếp nhận đơn của bạn");

            // md.showNotification("Duyệt đơn thành công","left","center","rose");
            window.location.reload(true);
        }
    })
});


