
$(document).ready(function() {
    var table1=$('#tablelskk').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
            ],
        buttons: [
            'excelHtml5', 'print'
        ],
        columnDefs:[{targets:[3,5], render:function(data){
            return moment(data).format('MMMM Do YYYY');
          }}],
    });
    $('#tablelskk').show();
    $('.yearselect').yearselect({
        start:2000,
        end:2019
    });
    $('.yearselect').change(function(){
        var data={};
        data.nam1=parseInt($('#namss1').val());
        data.nam2=parseInt($('#namss2').val());
        $.ajax({
            url:'/lskk/sosanh/'+data.nam1+'&'+data.nam2,
            type:'GET',
            data:JSON.stringify(data),
            contentType:'application/json',
            success:function(){
                document.location='/lskk/sosanh/'+data.nam1+'&'+data.nam2
            }
        })
    })
});
