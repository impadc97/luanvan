$(document).ready(function () {
    var slid;
    var currow;
    var tttl;
    // Setup - add a text input to each footer cell
    // $('#tablethietbi tfoot th').each( function () {
    //     var title = $(this).text();
    //     $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    // } );
    var tablethietbi = $('#tablethietbi').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'Show all']
        ],
        buttons: [
            'excelHtml5', 'print'
        ],
        select: true
    });
    $('#tablethietbi').show();
    $('#btn_themThietBi').on('click',function(){
        var soluong=parseInt($('#ipamount').val());
        var i;
        var cur;
        var idthietbi;
        var lastid;
        if(parseInt($('#soluong').text())!=0) // so luong thiet bi trong tai san ?
        {
            var lastrow=$('#tabledetail').find('tr:last');
            lastid=lastrow.find('td:last')[0].firstChild.textContent;
            cur = parseInt(lastid.substr(lastid.lastIndexOf('.')+1));
            if(isNaN(cur)) {
                cur = 0;
            }
            else {
                lastid=lastid.slice(0,lastid.lastIndexOf('.'));
            }
        }
        else 
        {
            lastid=$('#idthietbi').text(); // = 0 ma thiet bi dc init = ma tai san
            cur=0;
        }
        idthietbi=lastid+'.';
        var array=[];
        for(i=0;i<soluong;i++){
            var data={};
            var num=cur+i+1;
            data.id=idthietbi+num;
            data.TenThietBi=$('#ipTenTB').val();
            data.GiaTri=parseInt($('#ipGiaTriTB').val());
            data.MaDVSD=$('#DVSD').val();
            data.TS_MaTaiSan=$('#idthietbi').text();
            array.push(data);
        }
        var tmp={};
        tmp.array=array;
        $.ajax({
            url: '/thietbi',
            type: 'POST',
            data: JSON.stringify(tmp),
            contentType: 'application/json',
            success:function(){
                $.notify("Thêm thành công","success");
                location.reload();
            },
            error:function(){
                $.notify("Thêm thất bại","error");
            }
        })
    })
    // // Apply the search
    // tablethietbi.columns().every( function () {
    //     var that = this;
    //     $( 'input', this.footer() ).on( 'keyup change', function () {
    //         if ( that.search() !== this.value ) {
    //             that
    //                 .search(this.value)
    //                 .draw();
    //         }
    //     } );
    // } );

    //Add row button
    // $('.dt-add').each(function () {
    //     $(this).on('click', function (evt) {
    //         //Create some data and insert it
    //         var rowData = [];
    //         var table = $('#tablethietbi').DataTable();
    //         //Store next row number in array
    //         var info = table.page.info();
    //         rowData.push(info.recordsTotal + 1);
    //         //Some description
    //         rowData.push('New Order');
    //         //Random date
    //         var date1 = new Date(2016, 01, 01);
    //         var date2 = new Date(2018, 12, 31);
    //         var rndDate = new Date(+date1 + Math.random() * (date2 - date1));//.toLocaleDateString();
    //         rowData.push(rndDate.getFullYear() + '/' + (rndDate.getMonth() + 1) + '/' + rndDate.getDate());
    //         //Status column
    //         rowData.push('NEW');
    //         //Amount column
    //         rowData.push(Math.floor(Math.random() * 2000) + 1);
    //         //Inserting the buttons ???
    //         rowData.push('<button type="button" class="btn btn-primary btn-xs dt-edit" style="margin-right:16px;"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button><button type="button" class="btn btn-danger btn-xs dt-delete"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>');
    //         //Looping over columns is possible
    //         //var colCount = table.columns()[0].length;
    //         //for(var i=0; i < colCount; i++){			}

    //         //INSERT THE ROW
    //         table.row.add(rowData).draw(false);
    //     });
    // });


    $('#tablethietbi tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            tablethietbi.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            tmp = tablethietbi.row(this).data();
            currow = tablethietbi.row(this).index();
            slid = tmp[0].substring(tmp[0].indexOf('>') + 1, tmp[0].lastIndexOf('<'));
            tttl = tmp[2];
        }
    });
    function updateTB(updatedCell, updatedRow, oldValue) {
        if (updatedCell.data() != oldValue) {
            var result = confirm("Xác nhận sửa dữ liệu!");
            if (result == true) {
                var newvalue = updatedRow.data();
                var data = {};
                for (i = 0; i < newvalue.length; i++) {
                    if (newvalue[i] == '') newvalue[i] = null;
                }
                data.id = newvalue[0].substring(newvalue[0].indexOf('>') + 1, newvalue[0].lastIndexOf('<'));
                data.TenThietBi = newvalue[1];
                data.TinhTrangThanhLy = newvalue[2];
                data.NgayThanhLy = newvalue[3];
                data.NamSuDung = parseInt(newvalue[4]);
                data.GiaTri = parseInt(newvalue[5]);
                data.PhongBan = newvalue[6];
                data.TinhTrang = newvalue[7];
                $.ajax({

                    url: '/thietbi',
                    type: 'PUT',
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    error: function () {
                        updatedCell.data(oldValue);
                    }
                })
            }
            else {
                updatedCell.data(oldValue);
            }
        }
    }
    tablethietbi.MakeCellsEditable({
        "onUpdate": updateTB,
        "columns": [1, 2, 3, 4, 5, 6, 7],
        "inputTypes": [
            {
                "column": 1,
                "type": "text",
                "options": null
            },
            {
                "column": 2,
                "type": "text",
                "options": null
            },
            {
                "column": 3,
                "type": "datepicker", // requires jQuery UI: http://http://jqueryui.com/download/
                "options": {
                    "icon": "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif" // Optional
                }
            },
            {
                "column": 4,
                "type": "number",
                "options": null
            },
            {
                "column": 5,
                "type": "number",
                "options": null
            },
            {
                "column": 6,
                "type": "text",
                "options": null
            },
            {
                "column": 7,
                "type": "text",
                "options": null
            },
        ],
    });
    $('#btn-removetb').on('click', function () {
        var content = {};
        content.id = slid;
        $.ajax({
            url: '/thietbi',
            type: 'DELETE',
            data: JSON.stringify(content),
            contentType: 'application/json',
            sucess: function () {
                tablethietbi.row(currow).remove().draw();
            }
        })
    })
    //Delete buttons
    $('.dt-delete').each(function () {
        $(this).on('click', function (evt) {
            $this = $(this);
            var dtRow = $this.parents('tr');
            if (confirm("Bạn có muốn xoá thiết bị này?")) {
                // var table = $('#tablethietbi').DataTable();
                // table.row(dtRow[0].rowIndex - 1).remove().draw(false);

                var content = {};
                let Stmp = dtRow[0].cells[0].innerHTML;
                
                content.id = Stmp.substring(Stmp.indexOf('>') + 1, Stmp.lastIndexOf('<'));
                $.ajax({
                    url: '/thietbi',
                    type: 'DELETE',
                    data: JSON.stringify(content),
                    contentType: 'application/json',
                    sucess: function () {
                        tablethietbi.row(dtRow[0].rowIndex - 1).remove().draw();
                    }
                })
            }
        });
    });
    $('#btn-thanhlytb').on('click', function () {
        if (tttl != "true") {
            var content = {};
            content.id = slid;
            $.ajax({
                url: '/thietbi/thanhly',
                type: 'POST',
                data: JSON.stringify(content),
                contentType: 'application/json',
                success: function () {
                    $.ajax({
                        url: '/thietbi/lichsu',
                        type: 'post',
                        data: JSON.stringify(content),
                        contentType: 'application/json',
                        success: function (data) {
                            console.log(data);
                            tablethietbi.cell(currow, 2).data('true').draw();
                        }
                    })
                }
            })
        }
    })

    //Thanh ly row buttons
	$('.dt-thanhly').each(function () {
		$(this).on('click', function(evt){
			$this = $(this);
            var dtRow = $this.parents('tr');
            if(dtRow[0].cells[0].innerHTML != "true"){
                var content = {};
                let Stmp = dtRow[0].cells[0].innerHTML;

                content.id = Stmp.substring(Stmp.indexOf('>') + 1, Stmp.lastIndexOf('<'));
                $.ajax({
                    url: '/thietbi/thanhly',
                    type: 'POST',
                    data: JSON.stringify(content),
                    contentType: 'application/json',
                    success: function () {
                        $.ajax({
                            url: '/thietbi/lichsu',
                            type: 'post',
                            data: JSON.stringify(content),
                            contentType: 'application/json',
                            success: function (data) {
                                console.log(data);
                                tablethietbi.cell(dtRow[0].rowIndex - 1, 2).data('true').draw();
                            }
                        })
                    }
                })
            }
		});
	});
});