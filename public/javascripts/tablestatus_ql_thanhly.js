$(document).ready(function () {

    var InfoDon = document.getElementById('InfoDon');
    var idnguoixuly = InfoDon.dataset.idnguoixuly;
    var statusTable = document.getElementById('tablestatus');
    var curstatus = parseInt(statusTable.dataset.tinhtrang);

    //Kiem tra don da co ai xu ly chua
    //Neu chua co nguoi xu ly don
    if (!idnguoixuly) {

        $('.toggle-huydon').hide();
        $('#info-nguoixulydon').hide();
        $('#status-tracking-detail').hide();

    }
    else {
        $('#dt-duyetdon').hide();
    }
    //Neu don da bi huy
    if (curstatus == 0){
        $('#dt-duyetdon').hide();
        $('.toggle-huydon').hide();
        $('#status-tracking-detail').hide();
        $('#status-tracking').hide();
        $('#MaDon').append(`<b> (Đã hủy) </b>`);
    }

    //Xu ly table status theo trang thai don de nghi
    
    for (let i = 1; i < statusTable.rows.length; ++i) {
        if (i <= curstatus) {
            statusTable.rows[i].cells[2].innerHTML = `Hoàn tất`;

        }
        statusTable.rows[i].cells[2].style.cssText = "text-align:center;width:150px;height: auto;";
    }

    $('.dt-completestep').each(function () {
        $(this).on('click', function (evt) {
            let clickedStep = parseInt(this.dataset.step);

            if (clickedStep == curstatus + 1) {
                let nextStep = confirm("Bạn muốn hoàn tất bước này?");
                if (nextStep) {
                    let content = {}
                    //id don de nghi
                    let id = parseInt(document.getElementById("MaDon").dataset.madon);
                    if(clickedStep ==4) {
                        if($('#NgayThuHoi').val()==''){
                            alert('Bạn cần chọn ngày thu hồi!');
                            return;
                        }
                        content.NgayThuHoi=$('#NgayThuHoi').val();
                    }
                    content.id = id;
                    content.curstatus = curstatus;
                    content.signal=0;// nguoi gui la nhan vien qttb
                    $.ajax({
                        url: '/denghi/nextsteptly',
                        type: 'PATCH',
                        data: JSON.stringify(content),
                        contentType: 'application/json',
                        success: function (data) {
                            data.NoiDungThongBao='Có cập nhật của đơn đề nghị số ' +content.id;
                            $.ajax({
                                url:'/navbar',
                                type:'POST',
                                data:JSON.stringify(data),
                                contentType: 'application/json',
                                success:function(){
                                    $.notify("Cập nhật thành công", "success");
                                    location.reload();
                                },
                                error: function () {
                                    $.notify("Thêm thất bại", "error");
                                }
                            })
                        },
                        error: function () {
                            alert("that bai");
                        }
                    })
                }
            }
            else {
                alert("Bạn phải thực hiện các bước tuần tự !");
            }

        });
    });

    

    //In don de nghi buttons
    $('.dt-inmau').each(function () {
        $(this).on('click', e => {
            var url = "/print/mauso/" + this.dataset.mauso;
            window.open(url, "_blank");
        })
    });
    $('#btn_updateTLy').on('click',function(){
        var data={};
        data.madon=parseInt(document.getElementById("MaDon").dataset.madon);
        $.ajax({
            url:'/thietbi/tlyts', // url thanh ly nhieu thiet bi cung luc
            data:JSON.stringify(data),
            contentType: 'application/json',
            type:'PATCH',
            success:function(){
                $.notify("Cập nhật thành công", "success");
            },
            error: function () {
                $.notify("Thêm thất bại", "error");
            }
        })
    })
    $('#btn_themThietBi').on('click', function () {
        var taisan = {};
        taisan.id = ($('#ipMaTS').val());
        taisan.TenTaiSan = ($('#ipTenTB').val());
        taisan.NamSanXuat = ($('#ipNSX').val());
        taisan.ThoiHanBaoHanh = ($('#ipBH').val());
        var soluong = parseInt($('#ipamount').val());
        for (i = 0; i < soluong; i++) {
            var data = {};
            var num = cur + i + 1;
            data.id = idthietbi + num;
            data.TenThietBi = $('#ipTenTB').val();
            data.GiaTri = parseInt($('#ipGiaTriTB').val());
            data.MaDVSD = $('#DVSD').val();
            data.TS_MaTaiSan = $('#idthietbi').text();
            array.push(data);
        }
        var tmp = {};
        tmp.array = array;
        $.ajax({
            url: '/thietbi',
            type: 'POST',
            data: JSON.stringify(tmp),
            contentType: 'application/json',
            success: function () {
                $.notify("Thêm thành công", "success");
                location.reload();
            },
            error: function () {
                $.notify("Thêm thất bại", "error");
            }
        })
    })
});

//Duyet don denghi row buttons
$('#dt-duyetdon').click(function () {
    var content = {};
    //id don de nghi
    let id = parseInt(document.getElementById("MaDon").dataset.madon);

    content.id = id;
    $.ajax({
        url: '/denghi/duyetdon',
        type: 'PATCH',
        data: JSON.stringify(content),
        contentType: 'application/json',
        success: function () {

            //Thong bao cho donvisudung

            // var socket = io.connect("http://localhost:3000");

            // socket.emit('message-to-donvisudung', "Nhân viên đã tiếp nhận đơn của bạn");

            // md.showNotification("Duyệt đơn thành công","left","center","rose");
            window.location.reload(true);
        }
    })
});