// $(document).ready(function () {
//     var slid;
//     var tabledenghi=$('#tabledenghi-sohuu').DataTable({
//         dom: 'Blfrtip',
//         lengthMenu: [
//             [ 10, 25, 50, -1 ],
//             [ '10 rows', '25 rows', '50 rows', 'Show all' ]
//             ],
//         buttons: [

//         ],
//         processing : false,
//     });
//     $('#tabledenghi tbody').on( 'click', 'tr', function () {
//         if ( $(this).hasClass('selected') ) {
//             $(this).removeClass('selected');
//         }
//         else {
//             tabledenghi.$('tr.selected').removeClass('selected');
//             $(this).addClass('selected');
//             tmp=tabledenghi.row(this).data();
//             currow=tabledenghi.row(this).index();
//             slid=parseInt(tmp[0]);
//         }
//     } );

//     $('#btn_themDeNghi').on('click', function () {
//         var data = {};
//         data.noidunghoso = $('#NoiDung').val();
//         data.ngaydenghi = $('#NgayDeNghi').val();
$(document).ready(function () {
    var tablethietbi = $('#tablethietbi').DataTable({
        paging: false,
    });
    var dstb = new Array;
    $(".cbtb").click(function () {
        dstb = [];
        $('.cbtb:checked').each(function () {
            dstb.push($(this).val());
        });
    });


    $('#btn_exportWord').on('click', function () {
        var data = {};
        data.loaidon = $('#Loai').val();
        data.noidung = $('#NoiDung').val();
        data.nguoilienhe = $('#NguoiLienHe').val();
        data.sdt = $('#SDT').val();
        data.nguonkinhphi = $('#NguonKinhPhi').val();
        data.ngaydenghi = $('#NgayDeNghi').val();
        $.ajax({
            url: '/denghisohuu/export',
            type: 'post',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: function () {
                window.location.href = "export";
            },
            error: function () {
                $.notify("Thêm thất bại", "error");
            }
        })
    })

    //
    $('#timthietbi-area').hide();
    $('#Loai').on('change', function () {
        if (this.value == "2") {
            $('#timthietbi-area').show();
        }
        else {
            $('#timthietbi-area').hide();
        }
    });

    //Lay danh sach thiet bi
    var thietbis = new Array();
    $('#btn_timthietbi').on('click', function () {
        $.get("/thietbisohuu/json", function (data, status) {
            if (status == "success") {

                thietbis = data;
                let options = '';
                for (var i = 0; i < thietbis.length; i++) {
                    options += '<option value="' + thietbis[i].id + '" />';
                }
                document.getElementById('thietbis').innerHTML = options;
            }
        })
    })
    $('#thietbi-search').on('input', function () {

        let selectedId = this.value;
        for (let i = 0; i < thietbis.length; i++) {

            if (thietbis[i].id == selectedId) {
                let selectedItem = thietbis[i];
                $('#dialog-thietbi-tenthietbi').html(`<b>Tên thiết bị:</b> ${selectedItem.TenThietBi}`);
                $('#dialog-thietbi-namsudung').html(`<b>Năm sử dụng:</b> ${selectedItem.NamSuDung}`);
                $('#dialog-thietbi-giatri').html(`<b>Giá trị:</b> ${selectedItem.GiaTri}`);
                break;
            }
        }

    })

    $('#btn_themThietBi').on('click', function () {
        $("#Thietbi").val(`${$('[name="thietbis"]').val()}`);
    })

    $('#btn_themDeNghi').on('click', function () {
        var data = {};

        var loaidenghi = document.getElementById('Loai');
        var sdkp = document.getElementById('NguonKinhPhi');

        if (loaidenghi.value == "2") {
            var dataDondenghi = {};
            dataDondenghi.noidunghoso = $('#NoiDung').val();
            dataDondenghi.ngaydenghi = $('#NgayDeNghi').val();
            //data.ngaynhandenghi=$('#NgayNhanDeNghi').val();
            //data.namthuchien=$('#NamThucHien').val();
            dataDondenghi.loaidenghi = parseInt(loaidenghi.value);
            dataDondenghi.sdkp = sdkp.value;
            dataDondenghi.tinhtrang = 1;

            data.datadondenghi = dataDondenghi;
            data.idthietbicansua = $('#Thietbi').val();

            // if(data.ngaydenghi=="") data.ngaydenghi=null;
            // if(data.ngaynhandenghi=="") data.ngaynhandenghi=null;
            // if(data.namthuchien=="") data.namthuchien=null;
            // if(data.namthuchien!==data.namthuchien) data.namthuchien=null;
            //alert(data);
            //console.log(data);
            $.ajax({
                url: '/denghisohuu/addsuachua',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function () {
                    md.showNotification("Thêm thành công", 'bottom', 'right', 'rose');
                },
                error: function () {
                    md.showNotification("Thêm thất bại", 'bottom', 'right', 'rose');
                }
            });
        }
        else {
            data.noidunghoso = $('#NoiDung').val();
            data.ngaydenghi = $('#NgayDeNghi').val();
            //data.ngaynhandenghi=$('#NgayNhanDeNghi').val();
            //data.namthuchien=$('#NamThucHien').val();
            data.loaidenghi = parseInt(loaidenghi.value);
            data.sdkp = sdkp.value;
            data.tinhtrang = 1;

            // if(data.ngaydenghi=="") data.ngaydenghi=null;
            // if(data.ngaynhandenghi=="") data.ngaynhandenghi=null;
            // if(data.namthuchien=="") data.namthuchien=null;
            // if(data.namthuchien!==data.namthuchien) data.namthuchien=null;
            //alert(data);
            //console.log(data);
            $.ajax({
                url: '/denghisohuu/add',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function () {
                    md.showNotification("Thêm thành công", 'bottom', 'right', 'rose');
                },
                error: function () {
                    md.showNotification("Thêm thất bại", 'bottom', 'right', 'rose');
                }
            });
        }
    });

    $('#btn-taobannhap').on('click', function () {
        var data = {};

        var loaidenghi = document.getElementById('Loai');
        var sdkp = document.getElementById('NguonKinhPhi');

        if (loaidenghi.value == "2") {
            var dataDondenghi = {};
            dataDondenghi.noidunghoso = $('#NoiDung').val();
            dataDondenghi.ngaydenghi = $('#NgayDeNghi').val();
            //data.ngaynhandenghi=$('#NgayNhanDeNghi').val();
            //data.namthuchien=$('#NamThucHien').val();
            dataDondenghi.loaidenghi = parseInt(loaidenghi.value);
            dataDondenghi.sdkp = sdkp.value;
            dataDondenghi.tinhtrang = -1;

            data.datadondenghi = dataDondenghi;
            data.idthietbicansua = $('#Thietbi').val();

            // if(data.ngaydenghi=="") data.ngaydenghi=null;
            // if(data.ngaynhandenghi=="") data.ngaynhandenghi=null;
            // if(data.namthuchien=="") data.namthuchien=null;
            // if(data.namthuchien!==data.namthuchien) data.namthuchien=null;
            //alert(data);
            //console.log(data);
            $.ajax({
                url: '/denghisohuu/addsuachua',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function () {
                    md.showNotification("Thêm bản nháp thành công", 'bottom', 'right', 'rose');
                },
                error: function () {
                    md.showNotification("Thêm bản nháp thất bại", 'bottom', 'right', 'rose');
                }
            });
        }
        else {
            data.noidunghoso = $('#NoiDung').val();
            data.ngaydenghi = $('#NgayDeNghi').val();
            //data.ngaynhandenghi=$('#NgayNhanDeNghi').val();
            //data.namthuchien=$('#NamThucHien').val();
            data.loaidenghi = parseInt(loaidenghi.value);
            data.sdkp = sdkp.value;
            data.tinhtrang = -1;

            // if(data.ngaydenghi=="") data.ngaydenghi=null;
            // if(data.ngaynhandenghi=="") data.ngaynhandenghi=null;
            // if(data.namthuchien=="") data.namthuchien=null;
            // if(data.namthuchien!==data.namthuchien) data.namthuchien=null;
            //alert(data);
            //console.log(data);
            $.ajax({
                url: '/denghisohuu/add',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function () {
                    md.showNotification("Thêm bản nháp thành công", 'bottom', 'right', 'rose');
                },
                error: function () {
                    md.showNotification("Thêm bản nháp thất bại", 'bottom', 'right', 'rose');
                }
            });
        }
    });

    $('#btn-luu').on('click', function () {
        var data = {};

        var loaidenghi = document.getElementById('Loai');
        var sdkp = document.getElementById('NguonKinhPhi');

        if (loaidenghi.value == "2") {
            var dataDondenghi = {};
            dataDondenghi.noidunghoso = $('#NoiDung').val();
            dataDondenghi.ngaydenghi = $('#NgayDeNghi').val();
            //data.ngaynhandenghi=$('#NgayNhanDeNghi').val();
            //data.namthuchien=$('#NamThucHien').val();
            dataDondenghi.loaidenghi = parseInt(loaidenghi.value);
            dataDondenghi.sdkp = sdkp.value;
            dataDondenghi.tinhtrang = -1;

            data.datadondenghi = dataDondenghi;
            data.idthietbicansua = $('#Thietbi').val();

            // if(data.ngaydenghi=="") data.ngaydenghi=null;
            // if(data.ngaynhandenghi=="") data.ngaynhandenghi=null;
            // if(data.namthuchien=="") data.namthuchien=null;
            // if(data.namthuchien!==data.namthuchien) data.namthuchien=null;
            //alert(data);
            //console.log(data);
            $.ajax({
                url: '/denghisohuu/addsuachua',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function () {
                    md.showNotification("Thêm bản nháp thành công", 'bottom', 'right', 'rose');
                },
                error: function () {
                    md.showNotification("Thêm bản nháp thất bại", 'bottom', 'right', 'rose');
                }
            });
        }
        else {
            data.noidunghoso = $('#NoiDung').val();
            data.ngaydenghi = $('#NgayDeNghi').val();
            //data.ngaynhandenghi=$('#NgayNhanDeNghi').val();
            //data.namthuchien=$('#NamThucHien').val();
            data.loaidenghi = parseInt(loaidenghi.value);
            data.sdkp = sdkp.value;
            data.tinhtrang = -1;

            // if(data.ngaydenghi=="") data.ngaydenghi=null;
            // if(data.ngaynhandenghi=="") data.ngaynhandenghi=null;
            // if(data.namthuchien=="") data.namthuchien=null;
            // if(data.namthuchien!==data.namthuchien) data.namthuchien=null;
            //alert(data);
            //console.log(data);
            $.ajax({
                url: '/denghisohuu/add',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function () {
                    md.showNotification("Thêm bản nháp thành công", 'bottom', 'right', 'rose');
                },
                error: function () {
                    md.showNotification("Thêm bản nháp thất bại", 'bottom', 'right', 'rose');
                }
            });
        }
    });

    /**
     *  SOCKETIO HANDLE
     */

    var curUser = document.getElementById('current-user');
    //console.log(curUser.dataset.userid);

    var socket = io.connect("http://localhost:3000");

    //var room = `${curUser.dataset.userid}`;
    //socket.emit('test', "ok");

    //socket.on('message', function (data) {
    //    md.showNotification(data, 'bottom', 'right', 'rose');
    //});

    socket.on('message-receive-donvisudung', function (data) {
        md.showNotification(data, 'bottom', 'right', 'rose');
    });

    $('#Loai').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        if (valueSelected == 3) {
            $('#btn_TSTL').show();
        }
        else { $('#btn_TSTL').hide(); }
    })
})