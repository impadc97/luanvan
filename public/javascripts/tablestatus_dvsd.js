$(document).ready(function () {

    var InfoDon = document.getElementById('InfoDon');
    var idnguoixuly = InfoDon.dataset.idnguoixuly;
    var statusTable = document.getElementById('tablestatus');
    var curstatus = parseInt(statusTable.dataset.tinhtrang);

    //Kiem tra don da co ai xu ly chua
    //Neu chua co nguoi xu ly don
    // if (!idnguoixuly) {

    //     $('.toggle-huydon').hide();
    //     $('#info-nguoixulydon').hide();
    //     $('#status-tracking-detail').hide();

    // }
    // else {
    //     $('#dt-duyetdon').hide();
    // }
    //Neu don da bi huy
    if (curstatus == 0){
        $('#dt-duyetdon').hide();
        $('.toggle-huydon').hide();
        $('#status-tracking-detail').hide();
        $('#status-tracking').hide();
        $('#MaDon').append(`<b> (Đã hủy) </b>`);
    }

    //Xu ly table status theo trang thai don de nghi
    

    for (let i = 1; i < statusTable.rows.length; ++i) {
        if (i <= curstatus) {
            statusTable.rows[i].cells[2].innerHTML = `Hoàn tất`;

        }
        statusTable.rows[i].cells[2].style.cssText = "text-align:center;width:150px;height: auto;";
    }

    $('.dt-completestep').each(function () {
        $(this).on('click', function (evt) {
            let clickedStep = parseInt(this.dataset.step);

            if (clickedStep == curstatus + 1) {
                let nextStep = confirm("Bạn muốn hoàn tất bước này?");
                if (nextStep) {
                    let content = {}
                    //id don de nghi
                    let id = parseInt(document.getElementById("MaDon").dataset.madon);

                    content.id = id;
                    content.curstatus = curstatus;
                    $.ajax({
                        url: '/denghisohuu/nextstep',
                        type: 'PATCH',
                        data: JSON.stringify(content),
                        contentType: 'application/json',
                        success: function () {
                            window.location.reload(true);
                        },
                        error: function () {
                            alert("that bai");
                        }
                    });
                }
            }
            else {
                alert("Bạn phải thực hiện các bước tuần tự !");
            }

        });
    });

    // $('.dt-huydon').each(function () {
    //     $(this).on('click', function (evt) {
    //             let cancel = confirm("Bạn muốn hủy đơn?");
    //             if (cancel) {
    //                 let content = {}
    //                 //id don de nghi
    //                 let id = parseInt(document.getElementById("MaDon").dataset.madon);

    //                 content.id = id;
    //                 $.ajax({
    //                     url: '/denghisohuu/huydon',
    //                     type: 'PATCH',
    //                     data: JSON.stringify(content),
    //                     contentType: 'application/json',
    //                     success: function () {
    //                         window.location.reload(true);
    //                     },
    //                     error: function () {
    //                         alert("that bai");
    //                     }
    //                 });
    //         }
    //     });
    // });

    
    $('.dt-inmau').each(function () {
        $(this).on('click', e => {
            let url = "/print/mauso/" + this.dataset.mauso;
            window.open(url, "_blank");
        })
    });

    //In don de nghi buttons
    $('#btn-xemdondenghixetduyet').on('click',function(){
        let imgpath = this.dataset.imgpath;
        let url = 'http://localhost:3000/assests/img/dondenghi/banchoxetduyet/' + imgpath;
        $('#DocImg').attr("src",url);
    });

    //In don de nghi buttons
    $('#btn-xemdondenghifinal').on('click',function(){
        let imgpath = this.dataset.imgpath;
        let url = 'http://localhost:3000/assests/img/dondenghi/final/' + imgpath;
        alert(url);
        $('#DocImg').attr("src",url);
    });

    //Xem danh sach bao gia

    $('#btn-xembaogia').on('click',function(){
        let filestring = this.dataset.filepath;
        let dsBaogias = filestring.split(",");
        for( let i = 0 ; i < dsBaogias.length ; ++i){
            let url = 'http://localhost:3000/assests/doc/baogia/' + dsBaogias[i];
            $('#dsbaogia')
            .append(`<a href="${url}" download>${dsBaogias[i]}</a><br>`);
            $('#baogiaselect')
            .append(`<option value="${dsBaogias[i]}" selected>${dsBaogias[i]}</option>`);
        }
    });


    $("#btn-chonbaogia").on("click", function(){

        let baogiacontent = {};
        baogiacontent.selected = $('#baogiaselect').val();
        //id don de nghi
        let id = parseInt(document.getElementById("MaDon").dataset.madon);
        baogiacontent.id = id;

        $.ajax({
            url: '/denghisohuu/chonbaogia',
            type: 'PATCH',
            data: JSON.stringify(baogiacontent),
            contentType: 'application/json',
            success: function () {
                window.location.reload(true);
            },
            error: function () {
                alert("that bai");
            }
        });
    })
});