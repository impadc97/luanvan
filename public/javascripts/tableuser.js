$(document).ready(function () {
    var slid;
    var currow;
    var tttl;
    // Setup - add a text input to each footer cell
    // $('#tablethietbi tfoot th').each( function () {
    //     var title = $(this).text();
    //     $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    // } );
    var tableuser = $('#tableuser').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'Show all']
        ],
        buttons: [

        ],
        select: true
    });
    $('#tableuser').show();
    $('#btn_themUser').on('click', function () {
        var username = $('#username').val();
        var password = $('#password').val();
        var cfpwd = $('#cfpwd').val();
        var hoten = $('#name').val();
        var sdt = $('#phone').val();
        var email = $('#email').val();
        var thuocPhong = parseInt(document.getElementById("PhongBan").value);
        var role = parseInt(document.getElementById("role").value);
        console.log(password + " vaf " + cfpwd);
        if (username == '' || password == '' || cfpwd == '' || hoten == '' || sdt == '' || email == '') alert('Nhap thieu thong tin');
        else if (password != cfpwd) alert('Mat khau nhap lai khong chinh xac')
        else if (password.length < 4) alert('Mat khau qua ngan')
        else {
            var data = {};
            data.username = username;
            data.password = password;
            data.hoten = hoten;
            data.phone = sdt;
            data.email = email;
            data.role = role;
            data.idPhongban = thuocPhong;
            $.ajax({
                url: '/users/register',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function () {
                    $.notify("Thêm thành công", "success");
                },
                error: function () {
                    $.notify("Thêm thất bại", "error");
                }
            })
        }
        console.log(data);
    })



    $('#tableuser tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            tableuser.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            tmp = tableuser.row(this).data();
            currow = tableuser.row(this).index();
            slid = tmp[0].substring(tmp[0].indexOf('>') + 1, tmp[0].lastIndexOf('<'));
            tttl = tmp[2];
        }
    });
    function updateTB(updatedCell, updatedRow, oldValue) {
        if (updatedCell.data() != oldValue) {
            var result = confirm("Xác nhận sửa dữ liệu!");
            if (result == true) {
                var newvalue = updatedRow.data();
                var data = {};
                for (i = 0; i < newvalue.length; i++) {
                    if (newvalue[i] == '') newvalue[i] = null;
                }
                data.id = newvalue[0].substring(newvalue[0].indexOf('>') + 1, newvalue[0].lastIndexOf('<'));
                data.username = newvalue[1];
                data.password = newvalue[2];
                switch (newvalue[3]) {
                    case 'Nhân viên Qt. Thiết bị':
                        data.role = 2;
                        break;
                    case 'Trưởng đơn vị':
                        data.role = 1;
                        break;
                    default:
                        data.role = parseInt(newvalue[3]);
                        break;
                }

                data.idPhongban = parseInt(newvalue[4]);
                data.hoten = newvalue[5];
                data.phone = newvalue[6];
                data.email = newvalue[7];
                $.ajax({

                    url: '/admin/users',
                    type: 'PUT',
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    error: function () {
                        updatedCell.data(oldValue);
                    }
                })
            }
            else {
                updatedCell.data(oldValue);
            }
        }
    }
    tableuser.MakeCellsEditable({
        "onUpdate": updateTB,
        "columns": [1, 2, 3, 4, 5, 6, 7],
        "inputTypes": [
            {
                "column": 1,
                "type": "text",
                "options": null
            },
            {
                "column": 2,
                "type": "text",
                "options": null
            },
            {
                "column": 3,
                "type": "number", // requires jQuery UI: http://http://jqueryui.com/download/
                "options": {
                    "limit": "3" // Optional
                }
            },
            {
                "column": 4,
                "type": "number",
                "options": null
            },
            {
                "column": 5,
                "type": "text",
                "options": null
            },
            {
                "column": 6,
                "type": "text",
                "options": null
            },
            {
                "column": 7,
                "type": "text",
                "options": null
            },
        ],
    });
    $('#btn-removetb').on('click', function () {
        var content = {};
        content.id = slid;
        $.ajax({
            url: '/admin/users',
            type: 'DELETE',
            data: JSON.stringify(content),
            contentType: 'application/json',
            sucess: function () {
                tableuser.row(currow).remove().draw();
            }
        })
    })
    //Delete buttons
    $('.dt-delete').each(function () {
        $(this).on('click', function (evt) {
            $this = $(this);
            var dtRow = $this.parents('tr');
            if (confirm("Bạn có muốn xoá thiết bị này?")) {
                // var table = $('#tablethietbi').DataTable();
                // table.row(dtRow[0].rowIndex - 1).remove().draw(false);

                var content = {};
                let Stmp = dtRow[0].cells[0].innerHTML;

                content.id = Stmp.substring(Stmp.indexOf('>') + 1, Stmp.lastIndexOf('<'));
                $.ajax({
                    url: '/admin/users',
                    type: 'DELETE',
                    data: JSON.stringify(content),
                    contentType: 'application/json',
                    sucess: function () {
                        tableuser.row(dtRow[0].rowIndex - 1).remove().draw();
                    }
                })
            }
        });
    });

});