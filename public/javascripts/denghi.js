$(document).ready(function () {
    var slid;
    var tabledenghi_chuaxetduyet = $('#tabledenghi-chuaxetduyet').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'Show all']
        ],
        buttons: [

        ],
        processing: false,
    });
    // $('#tabledenghi-chuaxetduyet tbody').on( 'click', 'tr', function () {
    //     if ( $(this).hasClass('selected') ) {
    //         $(this).removeClass('selected');
    //     }
    //     else {
    //         tabledenghi_chaxetduet.$('tr.selected').removeClass('selected');
    //         $(this).addClass('selected');
    //         tmp=tabledenghi_chaxetduet.row(this).data();
    //         currow=tabledenghi_chaxetduet.row(this).index();
    //         slid=parseInt(tmp[0]);
    //     }
    // } );
    function updateDN(updatedCell, updatedRow, oldValue) {
        if (updatedCell.data() != oldValue) {
            var result = confirm("Xác nhận sửa dữ liệu!");
            if (result == true) {
                var newvalue = updatedRow.data();
                var data = {};
                for (i = 0; i < newvalue.length; i++) {
                    if (newvalue[i] == '') newvalue[i] = null;
                }
                data.id = parseInt(newvalue[0]);
                data.noidung = newvalue[1];
                data.ngaydenghi = newvalue[2];
                data.ngaynhandenghi = newvalue[3];
                data.namthuchien = newvalue[4];
                data.tinhtrang = parseInt(newvalue[5]);
                console.log(data);
                $.ajax({
                    url: '/denghi',
                    type: 'PUT',
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    error: function () {
                        updatedCell.data(oldValue);
                    }
                })
            }
            else {
                updatedCell.data(oldValue);
            }
        }
    }
    // tabledenghi_chaxetduet.MakeCellsEditable({
    //     "onUpdate":updateDN,
    //     "columns": [1,2,3,4,5],
    //     "inputTypes": [
    //         {
    //             "column": 1,
    //             "type": "text",
    //             "options": null
    //         },
    //         {
    //             "column":2, 
    //             "type": "datepicker",
    //             "options": {
    //                 "icon": "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif" // Optional
    //             }
    //         },
    //         {
    //             "column": 3,
    //             "type": "datepicker", // requires jQuery UI: http://http://jqueryui.com/download/
    //             "options": {
    //                 "icon": "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif" // Optional
    //             }
    //         },
    //         {
    //             "column":4, 
    //             "type": "number",
    //             "options": null
    //         },
    //         {
    //             "column":5, 
    //             "type": "number",
    //             "options":null
    //         },
    //     ],
    // })
    $('#btn_themDeNghi').on('click', function () {
        var data = {};
        data.id = parseInt($('#id').val());
        data.noidung = $('#NoiDung').val();
        data.ngaydenghi = $('#NgayDeNghi').val();
        data.ngaynhandenghi = $('#NgayNhanDeNghi').val();
        data.namthuchien = $('#NamThucHien').val();
        data.loaidenghi = parseInt(document.getElementById("Loai").value);
        if (data.ngaydenghi == "") data.ngaydenghi = null;
        if (data.ngaynhandenghi == "") data.ngaynhandenghi = null;
        if (data.namthuchien == "") data.namthuchien = null;
        if (data.namthuchien !== data.namthuchien) data.namthuchien = null;
        alert(data);
        console.log(data);
        $.ajax({
            url: '/denghi',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: function () {
                $.notify("Thêm thành công", "success");
            },
            error: function () {
                $.notify("Thêm thất bại", "error");
            }
        });
    });

    //Duyet don denghi row buttons
    $('.dt-duyetdondenghi').each(function () {
        $(this).on('click', function (evt) {
            $this = $(this);
            var content = {};
                let Stmp = this.getAttribute("data-iddenghi");

                content.id = Stmp;
                $.ajax({
                    url: '/denghi/duyetdon',
                    type: 'PATCH',
                    data: JSON.stringify(content),
                    contentType: 'application/json',
                    success: function () {

                        //Thong bao cho donvisudung

                        // var socket = io.connect("http://localhost:3000");

                        // socket.emit('message-to-donvisudung', "Nhân viên đã tiếp nhận đơn của bạn");
                        window.location.reload(true);
                    }
                })
        });
    });

    

})