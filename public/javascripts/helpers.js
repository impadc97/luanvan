var register = function (Handlebars) {
    var helpers = {
        switch: function (value, options) {
            this.switch_value = value;
            this.switch_break = false;
            return options.fn(this);
        },
        case: function (value, options) {
            if (value == this.switch_value) {
                this.switch_break = true;
                return options.fn(this);
            }
        },
        default: function (value, options) {
            if (this.switch_break == false) {
                return value;
            }
        },
        ifCond: function(v1, v2, options) {
            if(v1 === v2) {
              return options.fn(this);
            }
            return options.inverse(this);
          }
    };

    if (Handlebars && typeof Handlebars.registerHelper === "function") {
        for (var prop in helpers) {
            Handlebars.registerHelper(prop, helpers[prop]);
        }
    } else {
        return helpers;
    }

};

var now = function() {
    return new Date();
}

module.exports.now= now;
module.exports.register = register;
module.exports.helpers = register(null); 

    // Handlebars.registerHelper('switch', function (value, options) {
    //     this.switch_value = value;
    //     this.switch_break = false;
    //     return options.fn(this);
    // });

    // Handlebars.registerHelper('case', function (value, options) {
    //     if (value == this.switch_value) {
    //         this.switch_break = true;
    //         return options.fn(this);
    //     }
    // });

    // Handlebars.registerHelper('default', function (value, options) {
    //     if (this.switch_break == false) {
    //         return value;
    //     }
    // });
