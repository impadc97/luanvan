$(document).ready(function () 
{
    var InfoDon = document.getElementById('InfoDon');
    var idnguoixuly = InfoDon.dataset.idnguoixuly;
    var statusTable = document.getElementById('tablestatus');
    var curstatus = parseInt(statusTable.dataset.tinhtrang);
    if (curstatus == 0){
        $('#dt-duyetdon').hide();
        $('.toggle-huydon').hide();
        $('#status-tracking-detail').hide();
        $('#status-tracking').hide();
        $('#MaDon').append(`<b> (Đã hủy) </b>`);
    }
    if(curstatus==3){
        $('#btn_DongYLich').show();
        $('#btn_HuyLich').show();
    }
    else {
        $('#btn_DongYLich').hide();
        $('#btn_HuyLich').hide();
    }

    //Xu ly table status theo trang thai don de nghi
    

    for (let i = 1; i < statusTable.rows.length; ++i) {
        if (i <= curstatus) {
            statusTable.rows[i].cells[2].innerHTML = `Hoàn tất`;

        }
        statusTable.rows[i].cells[2].style.cssText = "text-align:center;width:150px;height: auto;";
    }
    $('#btn_DongYLich').on('click',function(){
        let content = {}
        let id = parseInt(document.getElementById("MaDon").dataset.madon);
        content.id = id;
        content.curstatus = curstatus;
        $.ajax({
            url: '/denghi/nextstep',
            type: 'PATCH',
            data: JSON.stringify(content),
            contentType: 'application/json',
            success: function () {
                window.location.reload(true);
            },
            error: function () {
                alert("that bai");
            }
        })
    })
    $('#btn_HuyLich').on('click',function(){
        var data = {}
        var id = parseInt(document.getElementById("MaDon").dataset.madon);
        data.madon = id;
        data.NgayThuHoi=null;
        $.ajax({
            url:'/denghisohuu/huylich',
            type:'PUT',
            data:JSON.stringify(data),
            success: function () {
                window.location.reload(true);
            },
            error: function () {
                alert("that bai");
            }
        })
    });
});