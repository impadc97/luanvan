function toggleSidebar(ref) {
    document.body.classList.toggle('sidebar-active');
}

$(document).ready(function () {
    var getLocation = function (href) {
        var l = document.createElement("a");
        l.href = href;
        return l;
    };
    var l = getLocation(window.location.href);
    var pathname = l.pathname.toString();
    var lblname;
    if(pathname.includes("taisan")){
        lblname = 'Quản lý tài sản';
    }else if(pathname.includes("thietbi")){
        lblname = 'Quản lý thiết bị';
    }
    else if(pathname.includes("denghi")){
        lblname = 'Quản lý đơn đề nghị';
    }
    else if(pathname.includes("admin/users")){
        lblname = 'Quản lý người dùng';
    }
    else if(pathname.includes("dxkk")){
        lblname = "Quản lý kết quả kiểm kê"; 
    }
    else{
        lblname='';
    }
    
    document.getElementById("navbar_label").innerHTML = lblname;
});
