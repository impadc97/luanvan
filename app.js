var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var favicon=require('serve-favicon');
var bodyParser=require('body-parser');
const session=require('express-session'); 
const expressValidator = require('express-validator');
const flash=require('connect-flash'); // thu vien passport ho tro authen
var passport=require('passport');



//secure route
var ensureAuthenticated = function(req, res, next) {
  if (req.isAuthenticated()) return next(); 
  else res.redirect('/users')
}


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var taisanRouter = require('./routes/taisan');
var thietbiRouter=require('./routes/thietbi');
var qltb_denghiRouter=require('./routes/qltb_denghi');
var thietbisohuuRouter = require('./routes/thietbisohuu');
var denghisohuuRouter = require('./routes/denghisohuu');
var adminRouter = require('./routes/admin');
var printRouter = require('./routes/print');
var bomonRouter = require('./routes/bomon');
var phongbanRouter = require('./routes/phongban');
var lskkRouter= require('./routes/lichsukk');
var lichsuRouter=require('./routes/lichsu');
var kiemkeRouter = require('./routes/kiemke');
var hbs=require('express-handlebars');
var Handlebars = require("handlebars");
var MomentHandler = require("handlebars.moment");

MomentHandler.registerHelpers(Handlebars);

var http = require('http');
var app = express();
var server = http.createServer(app);




app.use(session({ cookie: { maxAge: 30 * 60 * 1000 }, 
  secret: 'temp',
  resave: false, 
  saveUninitialized: false}));
app.use(flash()); 
// view engine setup
app.engine('hbs',hbs({extname:'hbs',defaultLayout:'layout',layoutsDir:__dirname+'/views/layouts/',helpers: require("./public/javascripts/helpers.js").helpers,partialsDir   : __dirname + '/views/partials/'}));
app.set('views', path.join(__dirname + '/views/'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session());
//define router
app.use('/users', usersRouter);
// app.use(ensureAuthenticated);
app.use('/denghi',qltb_denghiRouter);
app.use('/taisan',taisanRouter);
app.use('/thietbi',thietbiRouter);
app.use('/thietbisohuu',thietbisohuuRouter);
app.use('/denghisohuu',denghisohuuRouter);
app.use('/admin',adminRouter);
app.use('/print',printRouter);
app.use('/bomon',bomonRouter);
app.use('/phongban',phongbanRouter);
app.use('/lskk',lskkRouter);
app.use('/lichsu',lichsuRouter);
app.use('/dxkk',kiemkeRouter);
app.use('/', indexRouter);

var models = require('./models');
app.get('/sync', function(req, res){
	models.sequelize.sync().then(function(){
		res.send('database sync completed!');
	});
});
// app.listen(3000, function () {
//   console.log('Example app listening on port 3000!')
// })

server.listen(3000,function () {
  console.log('Example app listening on port 3000!')
});

/**
 * IO SOCKET HANDLING
 */

var io = require('socket.io').listen(server);
io.sockets.on('connection', function(socket){
  
  console.log("****************************************");
  console.log(`User: ${socket.id}`);
  console.log("****************************************");
  socket.on('room-request', (room) => {

    socket.join(room);
    io.sockets.in(room).emit('message', 'what is going on, party people?');
  })
  
  socket.on('test', (msg) => {

    io.emit('message', msg);
  })

  socket.on('message-to-donvisudung', (msg) => {

    socket.broadcast.emit('message-receive-donvisudung', msg);
  })

  socket.on('disconnect', function(){
    console.log('--> A connection is disconnected');
  });
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});
 
// passport
// express validator
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
    var namespace = param.split('.')
    , root = namespace.shift()
    , formParam = root
 
    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']'
    }
    return {
      param : formParam,
      msg : msg,
      value : value
    }
  }
}))
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;